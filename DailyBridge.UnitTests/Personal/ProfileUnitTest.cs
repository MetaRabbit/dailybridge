﻿using System.Threading.Tasks;


using Xunit;

namespace DailyBridge.UnitTests.Personal
{
    //public class ProfileUnitTest
    //{
        
    //    private Mock<IProfileRepository> _profileRepository;
    //    public ProfileUnitTest()
    //    {
    //        _profileRepository = new Mock<IProfileRepository>();
    //        _profileRepository.Setup(repository => repository.GetProfile(1)).ReturnsAsync(
    //             GeneralProfile.CreateProfile(new GeneralProfileId("1"), Email.CreateEmailFromString("test@test.net"), FullName.CreateFullName("fname", "fname", "lname"), PhoneNumber.CreatePhoneNumber("953_3793580")
    //            ));
    //    }
    //    /// <summary>
    //    /// create full profile
    //    /// </summary>
    //    [Fact]
    //    public void CreateFullProfile()
    //    {
    //        //arrange
    //        var fullName = FullName.CreateFullName("fname", "lname", "mname");
    //        var email = Email.CreateEmailFromString("test@test.ru");
    //        var phoneNumber = PhoneNumber.CreatePhoneNumber("953_3793580");
    //        //act
    //        var profile = GeneralProfile.CreateProfile(new GeneralProfileId("1"), email,
    //           fullName, phoneNumber);
    //        //assert
    //        Assert.Equal(profile.ProfileId.Id.Equals("1"), true);
    //        Assert.Equal(profile.Email.Equals(email), true);
    //        Assert.Equal(profile.PhoneNumber.Equals(phoneNumber), true);        
    //        Assert.Equal(profile.FullName.Equals(fullName), true);
    //    }
    //    /// <summary>
    //    /// Create min version profile: without middle name and phone number
    //    /// </summary>
    //    [Fact]
    //    public void CreateMinSizeProfile()
    //    {
    //        //arrange
    //        var fullName = FullName.CreateFullName("fname", "lname", null);
    //        var email = Email.CreateEmailFromString("test@test.ru");
    //        var phoneNumber = PhoneNumber.CreatePhoneNumber(null);
    //        //act
    //        var profile = GeneralProfile.CreateProfile(new GeneralProfileId("1"), email,
    //           fullName, phoneNumber);
    //        //assert
    //        Assert.Equal(profile.ProfileId.Id.Equals("1"), true);
    //        Assert.Equal(profile.Email.Equals(email), true);
    //        Assert.Equal(profile.FullName.MiddleName == "", true);
    //        Assert.Equal(profile.PhoneNumber==null, true);
            
    //        Assert.Equal(profile.FullName.Equals(fullName), true);
    //    }
    //    [Theory]
    //    [InlineData(null, "test@test.ru", "953_3793580", "fname", "lname")]
    //    [InlineData("1", "testtest.ru", "953_3793580", "fname", "lname", "mname")]
    //    [InlineData("-1", "tes@ttest.ru", "953_3793580", "fname", "lname", "mname")]
    //    [InlineData("1", "test@test.ru", "953_3793580",null, "lname", "mname")]
    //    [InlineData("1", "test@test.ru", "953_3793580", "fname",null, "mname")]
    //    [InlineData("1", "test@test.ru", "953_3793580", "fname", "window.alert(Foo);", "mname")]
    //    [InlineData("1", "test@test.ru", "953_3793580", "fn/ame", "lname", "mname")]
    //    [InlineData("a", "test@test.ru", "953_3793580", "fname", "lname", "mname")]

    //    [InlineData("1", "test@test.ru", "h", "fname", "lname")]
    //    [InlineData("1", "test@test.ru", "1111111111111111111111", "fname", "lname")]
    //    [InlineData("1", null, "953_3793580", "fname", "lname")]
    //    [InlineData("1", "test@test.ru", "9533793580", "fname", "lname")]
    //    public void CreateBadProfile(string id, string semail, string number, string fname, string lname, string mname)
    //    {
    //        //arrange
    //        var fullName = FullName.CreateFullName(fname, lname, mname);
    //        var email = Email.CreateEmailFromString(semail);
    //        var phoneNumber = PhoneNumber.CreatePhoneNumber(number);

    //        //act
    //        var profile = GeneralProfile.CreateProfile(new GeneralProfileId(id), email,
    //           fullName, phoneNumber);
    //        //assert
    //        Assert.Equal(profile.ProfileId.Id.Equals(id), true);
    //        Assert.Equal(profile.Email.Equals(email), true);
    //        Assert.Equal(profile.PhoneNumber.Equals(phoneNumber), true);
    //        Assert.Equal(profile.FullName.Equals(fullName), true);

    //    }
    //    [Theory]
    //    [InlineData("test@test.ru", "953_", "fname", "lname", "mname")]
    //    [InlineData("testtest.ru", "953_", "fname", "lname", "mname")]
    //    [InlineData(null, null, null, null, null)]
    //    [InlineData(null, null, null, null, "mn/ame")]
    //    public void BadChangeProfile(string semail,string number,string fname, string lname, string mname)
    //    {
    //        //arrange
    //        var fullName = FullName.CreateFullName(fname, lname, mname);
    //        var email = Email.CreateEmailFromString(semail);
    //        var phoneNumber = PhoneNumber.CreatePhoneNumber(number);
    //        var profile =  _profileRepository.Object.GetProfile(1).Result;
    //        //act
    //        profile.ChangeEmail(email);
    //        profile.ChangePhoneNumber(phoneNumber);
    //        profile.ChangeFullName(fullName);
    //        //assert
    //        Assert.Equal(profile.Email.Equals(email), true);
    //        Assert.Equal(profile.PhoneNumber.Equals(phoneNumber), true);
    //        Assert.Equal(profile.FullName.Equals(fullName), true);
    //    }
    //}
}
