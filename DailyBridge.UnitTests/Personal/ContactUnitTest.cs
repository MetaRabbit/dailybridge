﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


using Xunit;

namespace DailyBridge.UnitTests.Personal
{
    //public class ContactUnitTest
    //{
    //    private Mock<IProfileRepository> _profileRepository;
    //    private Mock<IContactRepository> _contactRepository;

    //    public ContactUnitTest()
    //    {
    //        _profileRepository = new Mock<IProfileRepository>();
    //        _contactRepository = new Mock<IContactRepository>();
    //        _profileRepository.Setup(repository => repository.GetProfile(1)).ReturnsAsync(
    //             GeneralProfile.CreateProfile(new GeneralProfileId("1"), Email.CreateEmailFromString("test@test.net"), FullName.CreateFullName("fname", "fname", "lname"), PhoneNumber.CreatePhoneNumber("953_3793580")
    //            ));
    //        _profileRepository.Setup(repository => repository.GetProfile(2)).ReturnsAsync(
    //             GeneralProfile.CreateProfile(new GeneralProfileId("2"), Email.CreateEmailFromString("test@test.net"), FullName.CreateFullName("fname", "fname", "lname"), PhoneNumber.CreatePhoneNumber("953_3793580")
    //            ));
    //        _contactRepository.Setup(repository => repository.GetContact(new GeneralProfileId("1"))).ReturnsAsync(            
    //             Contact.CreateContact(new ContactId(new GeneralProfileId("1"),new GeneralProfileId("2") ),TypeContact.Active)            
    //            );
    //    }
    //    [Fact]
    //    public void CreateActiveContact()
    //    {
    //        //arrange
    //        var profileSource = _profileRepository.Object.GetProfile(1).Result;
    //        var profileTarget = _profileRepository.Object.GetProfile(2).Result;
    //        //act
    //        var contact = Contact.CreateContact(new ContactId(profileSource.ProfileId, profileTarget.ProfileId), TypeContact.Active);
    //        //assert
    //        Assert.Equal(contact.ContactId.Equals(new ContactId(profileSource.ProfileId, profileTarget.ProfileId)), true);
    //        Assert.Equal(contact.Status, TypeContact.Active);

    //    }
    //    [Fact]
    //    public void CreateBlockContact()
    //    {
    //        //arrange
    //        var profileSource = _profileRepository.Object.GetProfile(1).Result;
    //        var profileTarget = _profileRepository.Object.GetProfile(2).Result;
    //        //act
    //        var contact = Contact.CreateContact(new ContactId(profileSource.ProfileId, profileTarget.ProfileId), TypeContact.Blocked);
    //        //assert
    //        Assert.Equal(contact.ContactId.Equals(new ContactId(profileSource.ProfileId, profileTarget.ProfileId)), true);
    //        Assert.Equal(contact.Status, TypeContact.Blocked);
    //    }
    //    [Fact]
    //    public void CreateFrontEndContact()
    //    {
    //        //arrange
    //        var profileSource = _profileRepository.Object.GetProfile(1).Result;
    //        var profileTarget = _profileRepository.Object.GetProfile(2).Result;
    //        //act
    //        var contact = Contact.CreateContact(new ContactId(profileSource.ProfileId, profileTarget.ProfileId), TypeContact.ActiveOnFrontEnd);
    //        //assert
    //        Assert.Equal(contact.ContactId.Equals(new ContactId(profileSource.ProfileId, profileTarget.ProfileId)), true);
    //        Assert.Equal(contact.Status, TypeContact.ActiveOnFrontEnd);
    //    }
    //    [Fact]
    //    public void SwitchStatusContact()
    //    {
    //        //arrange
    //        var contact = _contactRepository.Object.GetContact(new GeneralProfileId("1")).Result;
    //        //act         
    //        contact.Switch(TypeContact.Blocked);
    //        //assert
    //        Assert.Equal(contact.Status, TypeContact.Blocked);
    //    }

    //    [Theory]
    //    [InlineData("1", null)]
    //    [InlineData("1", "")]
    //    [InlineData(null, "2")]
    //    public void CreateBadContact(string idSource, string idTarget)
    //    {
    //        //arrange
    //        //act
    //        var contact = Contact.CreateContact(new ContactId(new GeneralProfileId(idSource), new GeneralProfileId(idTarget)), TypeContact.ActiveOnFrontEnd);
    //        //assert
    //        Assert.Equal(contact==null, false);
    //    }
    //}
}
