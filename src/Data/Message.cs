using System;
using System.Collections.Generic;

namespace Data
{
    public partial class Message
    {
        public int MessageId { get; set; }
        public string Data { get; set; }
        public int GeneralProfileIdSource { get; set; }
        public int GeneralProfileIdTarget { get; set; }
        public int? HistoryThemeId { get; set; }

        public virtual GeneralProfile GeneralProfileIdSourceNavigation { get; set; }
        public virtual GeneralProfile GeneralProfileIdTargetNavigation { get; set; }
        public virtual HistoryTheme HistoryTheme { get; set; }
    }
}
