﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Data.Entity;

namespace Data
{
    public interface IPersonalRepository
    {
        DbSet<Contact> Contact { get; set; }
        DbSet<GeneralProfile> GeneralProfile { get; set; }
        DbSet<HistoryTheme> HistoryTheme { get; set; }
        DbSet<Message> Message { get; set; }
        DbSet<Participation> Participation { get; set; }
        DbSet<PersonalEvent> PersonalEvent { get; set; }
        DbSet<PersonalLocation> PersonalLocation { get; set; }
        int SaveChanges();
        int SaveChanges(bool acceptAllChangesOnSuccess);
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken());
        
    }
}
