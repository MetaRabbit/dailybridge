using System;
using System.Collections.Generic;

namespace Data
{
    public partial class PersonalLocation
    {
        public PersonalLocation()
        {
            PersonalEvent = new HashSet<PersonalEvent>();
        }

        public int PersonalLocationId { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public int? GeneralProfileId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<PersonalEvent> PersonalEvent { get; set; }
        public virtual GeneralProfile GeneralProfile { get; set; }
    }
}
