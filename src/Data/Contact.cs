using System;
using System.Collections.Generic;

namespace Data
{
    public partial class Contact
    {
        public int Source { get; set; }
        public int Target { get; set; }
        public int Status { get; set; }

        public virtual GeneralProfile SourceNavigation { get; set; }
        public virtual GeneralProfile TargetNavigation { get; set; }
    }
}
