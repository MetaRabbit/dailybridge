using System;
using System.Collections.Generic;

namespace Data
{
    public partial class GeneralProfile
    {
        public GeneralProfile()
        {
            Contact = new HashSet<Contact>();
            ContactNavigation = new HashSet<Contact>();
            HistoryTheme = new HashSet<HistoryTheme>();
            Message = new HashSet<Message>();
            MessageNavigation = new HashSet<Message>();
            News = new HashSet<News>();
            Participation = new HashSet<Participation>();
            PersonalLocation = new HashSet<PersonalLocation>();
            SystemRequest = new HashSet<SystemRequest>();
        }

        public int GeneralProfileId { get; set; }
        public string AvatarUrl { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string PhoneNumber { get; set; }
        public int Status { get; set; }

        public virtual ICollection<Contact> Contact { get; set; }
        public virtual ICollection<Contact> ContactNavigation { get; set; }
        public virtual ICollection<HistoryTheme> HistoryTheme { get; set; }
        public virtual ICollection<Message> Message { get; set; }
        public virtual ICollection<Message> MessageNavigation { get; set; }
        public virtual ICollection<News> News { get; set; }
        public virtual ICollection<Participation> Participation { get; set; }
        public virtual ICollection<PersonalLocation> PersonalLocation { get; set; }
        public virtual ICollection<SystemRequest> SystemRequest { get; set; }
    }
}
