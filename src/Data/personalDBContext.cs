using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Metadata;

namespace Data
{
    public partial class PersonalDbContext : DbContext, IPersonalRepository
    {
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseNpgsql(@"Host=localhost;Database=PersonalDb;Username=postgres;Password=Rola1994");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Contact>(entity =>
            {
                entity.HasKey(e => new { e.Source, e.Target });

                entity.HasIndex(e => e.Source).HasName("Relationship_1_FK");

                entity.HasIndex(e => e.Target).HasName("Relationship_2_FK");

                entity.HasOne(d => d.SourceNavigation).WithMany(p => p.Contact).HasForeignKey(d => d.Source).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.TargetNavigation).WithMany(p => p.ContactNavigation).HasForeignKey(d => d.Target).OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<GeneralProfile>(entity =>
            {
                entity.Property(e => e.GeneralProfileId).ValueGeneratedNever();

                entity.Property(e => e.AvatarUrl)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasColumnType("varchar");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnType("varchar");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(20)
                    .HasColumnType("varchar");

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(20)
                    .HasColumnType("varchar");

                entity.Property(e => e.MiddleName)
                    .HasMaxLength(20)
                    .HasColumnType("varchar");

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(20)
                    .HasColumnType("varchar");
            });

            modelBuilder.Entity<HistoryTheme>(entity =>
            {
                entity.HasIndex(e => e.GeneralProfileId).HasName("Relationship_history_FK");

                entity.Property(e => e.Description)
                    .HasMaxLength(200)
                    .HasColumnType("varchar");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnType("varchar");

                entity.HasOne(d => d.GeneralProfile).WithMany(p => p.HistoryTheme).HasForeignKey(d => d.GeneralProfileId).OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<Message>(entity =>
            {
                entity.HasIndex(e => e.GeneralProfileIdSource).HasName("Relationship_mesWriter_FK");

                entity.HasIndex(e => e.GeneralProfileIdTarget).HasName("Relationship_mesRead_FK");

                entity.HasIndex(e => e.HistoryThemeId).HasName("Relationship_8_FK");

                entity.Property(e => e.Data)
                    .IsRequired()
                    .HasMaxLength(500)
                    .HasColumnType("varchar");

                entity.HasOne(d => d.GeneralProfileIdSourceNavigation).WithMany(p => p.Message).HasForeignKey(d => d.GeneralProfileIdSource).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.GeneralProfileIdTargetNavigation).WithMany(p => p.MessageNavigation).HasForeignKey(d => d.GeneralProfileIdTarget).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.HistoryTheme).WithMany(p => p.Message).HasForeignKey(d => d.HistoryThemeId);
            });

            modelBuilder.Entity<News>(entity =>
            {
                entity.HasIndex(e => e.AuthorId).HasName("Relationship_15_FK");

                entity.HasIndex(e => e.PersonalEventId).HasName("Relationship_14_FK");

                entity.Property(e => e.Heading)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasColumnType("varchar");

                entity.Property(e => e.SubHeading)
                    .HasMaxLength(200)
                    .HasColumnType("varchar");

                entity.Property(e => e.Text)
                    .IsRequired()
                    .HasMaxLength(1000)
                    .HasColumnType("varchar");

                entity.HasOne(d => d.Author).WithMany(p => p.News).HasForeignKey(d => d.AuthorId).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.PersonalEvent).WithMany(p => p.News).HasForeignKey(d => d.PersonalEventId);
            });

            modelBuilder.Entity<Participation>(entity =>
            {
                entity.HasKey(e => new { e.GeneralProfileId, e.PersonalEventId });

                entity.HasIndex(e => e.GeneralProfileId).HasName("Relationship_10_FK");

                entity.HasIndex(e => e.PersonalEventId).HasName("Relationship_11_FK");

                entity.Property(e => e.Comment)
                    .HasMaxLength(50)
                    .HasColumnType("varchar");

                entity.HasOne(d => d.GeneralProfile).WithMany(p => p.Participation).HasForeignKey(d => d.GeneralProfileId).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.PersonalEvent).WithMany(p => p.Participation).HasForeignKey(d => d.PersonalEventId);
            });

            modelBuilder.Entity<PersonalEvent>(entity =>
            {
                entity.HasIndex(e => e.PersonalLocationId).HasName("Relationship_12_FK");

                entity.Property(e => e.AvatarUrl)
                    .HasMaxLength(255)
                    .HasColumnType("varchar");

                entity.Property(e => e.Description)
                    .HasMaxLength(2000)
                    .HasColumnType("varchar");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnType("varchar");

                entity.HasOne(d => d.PersonalLocation).WithMany(p => p.PersonalEvent).HasForeignKey(d => d.PersonalLocationId).OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<PersonalLocation>(entity =>
            {
                entity.HasIndex(e => e.GeneralProfileId).HasName("Relationship_3_FK");

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnType("varchar");

                entity.Property(e => e.Description)
                    .HasMaxLength(500)
                    .HasColumnType("varchar");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnType("varchar");

                entity.HasOne(d => d.GeneralProfile).WithMany(p => p.PersonalLocation).HasForeignKey(d => d.GeneralProfileId);
            });

            modelBuilder.Entity<SystemRequest>(entity =>
            {
                entity.HasIndex(e => e.GeneralProfileId).HasName("Relationship_13_FK");

                entity.Property(e => e.SystemRequestId).ValueGeneratedNever();

                entity.Property(e => e.Description)
                    .HasMaxLength(200)
                    .HasColumnType("varchar");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(20)
                    .HasColumnType("varchar");

                entity.HasOne(d => d.GeneralProfile).WithMany(p => p.SystemRequest).HasForeignKey(d => d.GeneralProfileId).OnDelete(DeleteBehavior.Restrict);
            });
        }

        public virtual DbSet<Contact> Contact { get; set; }
        public virtual DbSet<GeneralProfile> GeneralProfile { get; set; }
        public virtual DbSet<HistoryTheme> HistoryTheme { get; set; }
        public virtual DbSet<Message> Message { get; set; }
        public virtual DbSet<News> News { get; set; }
        public virtual DbSet<Participation> Participation { get; set; }
        public virtual DbSet<PersonalEvent> PersonalEvent { get; set; }
        public virtual DbSet<PersonalLocation> PersonalLocation { get; set; }
        public virtual DbSet<SystemRequest> SystemRequest { get; set; }
    }
}