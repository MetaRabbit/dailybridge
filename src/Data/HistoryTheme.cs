using System;
using System.Collections.Generic;

namespace Data
{
    public partial class HistoryTheme
    {
        public HistoryTheme()
        {
            Message = new HashSet<Message>();
        }

        public int HistoryThemeId { get; set; }
        public string Description { get; set; }
        public int GeneralProfileId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Message> Message { get; set; }
        public virtual GeneralProfile GeneralProfile { get; set; }
    }
}
