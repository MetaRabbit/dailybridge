﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Extends
{
    public static class Extends
    {
        public static bool NotExists(this News news)
        {
            return news == null;
        }
        public static bool NotExists(this PersonalEvent news)
        {
            return news == null;
        }
        public static bool NotExists(this GeneralProfile news)
        {
            return news == null;
        }
        public static bool NotExists(this PersonalLocation news)
        {
            return news == null;
        }
    }
}
