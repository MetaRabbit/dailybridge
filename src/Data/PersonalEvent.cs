using System;
using System.Collections.Generic;

namespace Data
{
    public partial class PersonalEvent
    {
        public PersonalEvent()
        {
            News = new HashSet<News>();
            Participation = new HashSet<Participation>();
        }

        public int PersonalEventId { get; set; }
        public string AvatarUrl { get; set; }
        public string Description { get; set; }
        public bool IsPerodicity { get; set; }
        public string Name { get; set; }
        public int PersonalLocationId { get; set; }
        public long TimeOffset { get; set; }
        public int Type { get; set; }

        public virtual ICollection<News> News { get; set; }
        public virtual ICollection<Participation> Participation { get; set; }
        public virtual PersonalLocation PersonalLocation { get; set; }
    }
}
