using System;
using System.Collections.Generic;

namespace Data
{
    public partial class Participation
    {
        public int GeneralProfileId { get; set; }
        public int PersonalEventId { get; set; }
        public string Comment { get; set; }
        public int Type { get; set; }

        public virtual GeneralProfile GeneralProfile { get; set; }
        public virtual PersonalEvent PersonalEvent { get; set; }
    }
}
