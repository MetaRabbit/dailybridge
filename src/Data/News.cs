using System;
using System.Collections.Generic;

namespace Data
{
    public partial class News
    {
        public int NewsId { get; set; }
        public int AuthorId { get; set; }
        public string Heading { get; set; }
        public int? PersonalEventId { get; set; }
        public int Status { get; set; }
        public string SubHeading { get; set; }
        public string Text { get; set; }
        public long TimeOffset { get; set; }
        public int Type { get; set; }
        public int TypeNotify { get; set; }

        public virtual GeneralProfile Author { get; set; }
        public virtual PersonalEvent PersonalEvent { get; set; }
    }
}
