using System;
using System.Collections.Generic;

namespace Data
{
    public partial class SystemRequest
    {
        public int SystemRequestId { get; set; }
        public string Description { get; set; }
        public int GeneralProfileId { get; set; }
        public string Name { get; set; }
        public int TemplateId { get; set; }
        public int Type { get; set; }

        public virtual GeneralProfile GeneralProfile { get; set; }
    }
}
