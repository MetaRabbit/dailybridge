﻿using System.IO;
using DailyBridge.Domain.Interface.Service;
using DailyBridge.Domain.Repository;
using DailyBridge.Infrastruction;
using DailyBridge.Infrastruction.Interface;
using DailyBridge.Infrastruction.Synchronizer;
using Data;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.PlatformAbstractions;
using Serilog;
using Serilog.Sinks.RollingFile;

namespace DailyBridge
{
    public class Startup
    {
        public Startup(IHostingEnvironment env, IApplicationEnvironment envApp)
        {
            // Set up configuration sources.
            var builder = new ConfigurationBuilder()
                .AddJsonFile("config.json");

            if (env.IsEnvironment("Development"))
            {
                // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
                //builder.AddApplicationInsightsSettings(developerMode: true);
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build().ReloadOnChanged("config.json");
            Log.Logger = new LoggerConfiguration()
           .MinimumLevel.Verbose()
           .WriteTo.RollingFile(Path.Combine(
               envApp.ApplicationBasePath, "log-{Date}.txt"), 
               outputTemplate:
            "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} {Level}:{EventId} [{SourceContext}] {Message}{NewLine}{Exception}")
           .CreateLogger();

        }

        public IConfigurationRoot Configuration { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            //services.AddApplicationInsightsTelemetry(Configuration);

            services.AddMvc();
            services.AddSingleton<IGeneralProfileRepository, GeneralProfileRepository>();
            services.AddSingleton<IGeneralProfileQueryService, GeneralProfileQueryService>();        
            services.AddSingleton<ICacheSynchProfileService, CacheSynchProfileService>();

            services.AddSingleton<IContactQueryService, ContactQueryService>();
            services.AddSingleton<ICacheSynchContactService, CacheSynchContactService>();

            services.AddSingleton<IPersonalLocationQueryService, PersonalLocationQueryService>();
            services.AddSingleton<ICachSynchLocationService, CacheSynchLocationService>();

            services.AddSingleton<IPersonalEventQueryService, PersonalEventQueryService>();
            services.AddSingleton<ICachSynchPersonalEventService, CachSynchPersonalEventService>();

            services.AddSingleton<IPartipationQueryService, PartipationQueryService>();

            services.AddSingleton<INewsQueryService, PersonalNewsQueryService>();
            services.AddSingleton<ICachSyncPersonalNewsService, CachSyncPersonalNewsService>();

            services.AddSingleton<IPersonalRepository, PersonalDbContext>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            // loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            //loggerFactory.AddDebug();
            loggerFactory.AddSerilog();
            loggerFactory.AddDebug();
            loggerFactory.AddConsole();
            app.UseIISPlatformHandler();

            
            app.UseBrowserLink();
            app.UseDeveloperExceptionPage();
            app.UseDatabaseErrorPage();
            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Screen}/{action=Index}/{id?}");
            });
           
        }
        // Entry point for the application.
        public static void Main(string[] args) => WebApplication.Run<Startup>(args);
    }
}
