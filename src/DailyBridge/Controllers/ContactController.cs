﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DailyBridge.Domain.Entity;
using DailyBridge.Domain.Identity;
using DailyBridge.Domain.Interface.Entity;
using DailyBridge.Domain.Interface.Enumeration;
using DailyBridge.Domain.Interface.Service;
using DailyBridge.Domain.Utils;
using DailyBridge.Infrastruction.Interface;
using DailyBridge.Utils;
using Microsoft.AspNet.Mvc;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace DailyBridge.Controllers
{
    [Route("api/[controller]")]
    public class ContactController : Controller
    {
        private readonly IContactQueryService _contactQuaryService;
        private readonly IGeneralProfileRepository _profileRepository;
        private IGeneralProfileQueryService _generalProfileQueryService;
        private readonly ICacheSynchContactService _cacheSynchContactService;

        public ContactController(IContactQueryService contactQuaryService, IGeneralProfileRepository profileRepository, ICacheSynchContactService cacheSynchContactService, IGeneralProfileQueryService generalProfileQueryService)
        {
            _contactQuaryService = contactQuaryService;
            _profileRepository = profileRepository;
            _cacheSynchContactService = cacheSynchContactService;
            _generalProfileQueryService = generalProfileQueryService;
        }

        /// <summary>
        /// Метод возвращает список контактов. Отображаемые контакты синхронизируются с поисковым движком вместе с профилем.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <param name="numPage"></param>
        /// <param name="countProfileOnPage"></param>
        /// <returns></returns>
        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<JsonResult> Get(string id, string status, int numPage, int countProfileOnPage)
        {
            TypeContact typeContact;
            if (!Enum.TryParse(status, true, out typeContact) || !Validator.VerifySearchResultSize(numPage, countProfileOnPage) || !Validator.VerifyProfileId(id))
            {
                Response.BadRequest();
                return null;
            }

            IEnumerable<Data.GeneralProfile> profiles=null;
            if (typeContact == TypeContact.ActiveOnFrontEnd)
            {
                var frontContactList = await _contactQuaryService.SearchContactsListAsync(Convert.ToInt32(id), numPage, countProfileOnPage);
                var listIds = (frontContactList.Select(contact => contact.Target)).ToArray();
                profiles = await _generalProfileQueryService.SearchProfilesAsync(listIds, numPage, countProfileOnPage);
                return Json(profiles);
            }

            var contactList =
                           await
                               _contactQuaryService.GetContactsListAsync(Convert.ToInt32(id), (int)typeContact, numPage,
                                   countProfileOnPage);

            profiles = await _generalProfileQueryService.GetProfilesAsync((contactList.Select(contact => contact.Target)).ToArray());

            return Json(profiles);

        }

        // POST api/values
        [HttpPost]
        public void Post(int id, int target, string status)
        {
            //Проверка входных данных 
            var contactId = ContactId.Create(GeneralProfileId.Create(id.ToString()),
                GeneralProfileId.Create(target.ToString()));
            TypeContact typeContact;

            if (!Enum.TryParse(status, true, out typeContact) || contactId == null || !_profileRepository.Exists(contactId))
            {
                Response.BadRequest();
                return;
            }
            var contact = _profileRepository.GetContactAsync(contactId).Result;

            contact.Switch(typeContact);
            //Обновление данных в источниках 
            _profileRepository.UpdateAsync(contact).Wait();
            _cacheSynchContactService.SyncContact(contact.ContactId.SourceId.Id, contact.ContactId.TargetId.Id,(int)contact.Status);

        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(string id, string target)
        {
            //TODO Проверка возможности добавить в контакт
            IContact contact = Contact.CreateContact(ContactId.Create(GeneralProfileId.Create(id),
                GeneralProfileId.Create(target)), TypeContact.Active);

            //Проверка целостности данных
            if (!_profileRepository.TryAddContactAsync(contact).Result)
            {
                Response.BadRequest();
            }
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(string id, string target)
        {

            var profileIdSource = GeneralProfileId.Create(id);
            var profileIdTarget = GeneralProfileId.Create(target);
            var contact = ContactId.Create(profileIdSource, profileIdTarget);

            if (contact == null || !_profileRepository.Exists(contact))
            {
                Response.BadRequest();
                return;
            }
            _profileRepository.DeleteAsync(contact);

            //if (_contactRepository.Contains(contact).Result)
            //{
            //    _contactRepository.DeleteContact(contact);
            //}
            //else
            //{
            //    Response.BadRequest();
            //}
        }
    }
}
