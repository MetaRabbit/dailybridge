﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DailyBridge.Domain.Identity;
using DailyBridge.Domain.Interface.Service;
using DailyBridge.Domain.ValueObject;
using DailyBridge.Infrastruction.Interface;
using DailyBridge.Utils;
using Data;
using Microsoft.AspNet.Mvc;
using Microsoft.Extensions.Logging;

namespace DailyBridge.Controllers
{
    [Route("api/[controller]")]
    public class GeneralProfileController : Controller
    {
        private readonly IGeneralProfileRepository _profilRepository;
        private readonly IGeneralProfileQueryService _generalProfileQuaryService;
        private readonly ICacheSynchProfileService _cacheSynchProfileService;
        private readonly ILogger<GeneralProfileController> _logger;

        public GeneralProfileController(IGeneralProfileRepository profilRepository,
            IGeneralProfileQueryService generalProfileQuaryService, ILogger<GeneralProfileController> logger,
            ICacheSynchProfileService cacheSynchProfileService)
        {
            _profilRepository = profilRepository;
            _generalProfileQuaryService = generalProfileQuaryService;
            _logger = logger;
            _cacheSynchProfileService = cacheSynchProfileService;
        }

        // GET: api/values
        [HttpGet]
        public async Task<JsonResult> Get(string text, int numPage, int countProfileOnPage, int[] selectedUserIds)
        {
            //TODO исключение из списка инициатора запроса
            var profiles =
                await
                    _generalProfileQuaryService.SearchProfilesAsync(text, selectedUserIds, numPage, countProfileOnPage);
            Console.WriteLine(profiles);
            return Json(profiles);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<JsonResult> Get(int id)
        {
            //TODO Контроль возврата полного/частичного профиля
            try
            {
                var profile = await _generalProfileQuaryService.GetProfileAsync(id, false);

                if (profile != null) return Json(profile);

                if (await _cacheSynchProfileService.TrySyncDbAsync(id, false))
                {
                    profile = await _generalProfileQuaryService.GetProfileAsync(id, false);
                    if (profile != null) return Json(profile);
                    //Ошибка работы с поисковым ботом.
                    Response.ServerErrorRequest();
                    return null;
                }
                Response.NotFoundRequest();
                return null;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }


        }

        // POST api/values
        [HttpPost]
        public async Task Post([FromBody] GeneralProfile profile)
        {
            Console.WriteLine(Request);
            _logger.LogVerbose("Post ЗАПРОС");

            var profileData =
                await
                    _profilRepository.GetProfileAsync(
                        GeneralProfileId.Create(profile.GeneralProfileId.ToString()));

            if (profileData == null)
            {
                Response.BadRequest();
                return;
            }
            profileData.ChangeFullName(FullName.CreateFullName(profile.FirstName, profile.LastName,
                profile.MiddleName))
                .ChangePhoneNumber(PhoneNumber.CreatePhoneNumber(profile.PhoneNumber));

            await _profilRepository.UpdateAsync(profileData);
            _cacheSynchProfileService.SyncData(profile);
        }

        /// <summary>
        /// need first name, email, lastname, id
        /// </summary>
        /// <param name="prof"></param>
        // PUT api/values/5
        [HttpPut]
        public void Put([FromBody] GeneralProfile prof)
        {
            //if (prof.FirstName!=null && prof.Email != null && prof.LastName != null && prof.GeneralProfileId != null)
            //{
            //    Response.BadRequest();
            //    return;
            //}
            _logger.LogVerbose("Пользователь пришел: {0} {1} {2} ", prof.FirstName, prof.MiddleName, prof.LastName);
            var profile =
                Domain.Entity.GeneralProfile.CreateProfile(
                    GeneralProfileId.Create(prof.GeneralProfileId.ToString()),
                    Email.CreateEmailFromString(prof.Email),
                    FullName.CreateFullName(prof.FirstName, prof.LastName, prof.MiddleName),
                    PhoneNumber.CreatePhoneNumber(prof.PhoneNumber));
            if (profile == null)
            {
                Response.BadRequest();
                return;
            }
            var success = _profilRepository.AddAsync(profile).Result;           
            if (success)
            {
                _cacheSynchProfileService.SyncData(prof);
                _logger.LogVerbose("Пользователь создан: {0} {1} {2} ", profile.FullName.FirstName,
                    profile.FullName.MiddleName, profile.FullName.LastName);
            }
            else
            {
                Response.BadRequest();
            }

       }
    
        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
