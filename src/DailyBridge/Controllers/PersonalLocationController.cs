﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DailyBridge.Domain.Identity;
using DailyBridge.Domain.Interface.Service;
using DailyBridge.Domain.Service;
using DailyBridge.Domain.ValueObject;
using DailyBridge.Infrastruction.Interface;
using DailyBridge.Utils;
using Data;
using Microsoft.AspNet.Mvc;
using Microsoft.Extensions.Logging;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace DailyBridge.Controllers
{
    [Route("api/[controller]")]
    public class PersonalLocationController : Controller
    {
        private readonly IGeneralProfileRepository _profilRepository;
        private readonly IPersonalLocationQueryService _personalLocationQueryService;
        private readonly ICachSynchLocationService _cachSynchLocationService;
        private readonly ILogger<GeneralProfileController> _logger;

        public PersonalLocationController(IGeneralProfileRepository profilRepository, IPersonalLocationQueryService personalLocationQueryService, ICachSynchLocationService cachSynchLocationService, ILogger<GeneralProfileController> logger)
        {
            _profilRepository = profilRepository;
            _personalLocationQueryService = personalLocationQueryService;
            _cachSynchLocationService = cachSynchLocationService;
            _logger = logger;
        }

        // GET: api/PersonalLocation?profId=10000&text=тест
        public async Task<JsonResult> Get(int profId, string text)
        {
            try
            {
                var locations = await _personalLocationQueryService.SearchPersonalLocationsAsync(text, profId.ToString());

                if (locations != null) return Json(locations);
                Response.BadRequest();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Response.BadRequest();
            }
            return null;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<JsonResult> Get(int id)
        {
            try
            {
                var location = await _personalLocationQueryService.SearchPersonalLocationAsync(id.ToString());
                if (location != null) return Json(location);

                if (await _cachSynchLocationService.SyncLocationById(id))
                {
                    location = await _personalLocationQueryService.SearchPersonalLocationAsync(id.ToString());
                    return Json(location);
                }
                Response.NotFoundRequest();
                return null;
            }
            catch (IOException e)
            {
                _logger.LogError("ES is down: "+e.Message+": "+e.StackTrace);
                var loc = await _personalLocationQueryService.GetPersonalLocationAsync(id);
                return Json(loc);
            }
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]PersonalLocation data)
        {
            var locId = LocationId.Create(data.PersonalLocationId.ToString());
            if (locId == null)
            {
                Response.BadRequest();
                return;
            }
            var loc = _profilRepository.GetLocationAsync(locId).Result;
            if (loc == null)
            {
                Response.BadRequest();
                return;
            }

            loc.ChangeLocationInformation(LocationInformation.CreateLocationInfo(data.Name, data.Description))
               .ChangeAddress(data.Address);

            _profilRepository.UpdateAsync(loc);
            _cachSynchLocationService.SyncLocationById(Convert.ToInt32(loc.LocationId.Id));

        }

        // PUT api/values/5
        [HttpPut]
        public void Put([FromBody]PersonalLocation data)
        {
            if (data == null)
            {
                Response.BadRequest();
                return;
            }

            var domainObject = GeneralProfileConvertService.ConvertToDomainOject(data);

            var success = _profilRepository.AddAsync(domainObject).Result;

            if (!success)
            {
                Response.BadRequest();
            }

        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id,int profId )
        {
            var domainLocId = LocationId.Create(id.ToString());
            var domainProfId = GeneralProfileId.Create(profId.ToString());

            if (domainLocId == null || domainProfId==null)
            {
                Response.BadRequest();
                return;
            }
            _profilRepository.DeleteAsync(domainProfId,domainLocId );

            _cachSynchLocationService.SyncLocationsByProfileId(domainProfId.Id);
        }
    }
}
