﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DailyBridge.Domain.Identity;
using DailyBridge.Domain.Interface.Enumeration;
using DailyBridge.Domain.Interface.Service;
using DailyBridge.Domain.Service;
using DailyBridge.Domain.ValueObject;
using DailyBridge.Infrastruction.Interface;
using DailyBridge.Infrastruction.Utils;
using DailyBridge.Utils;
using Data;
using Data.Extends;
using Microsoft.AspNet.Mvc;

namespace DailyBridge.Controllers
{
    [Route("api/[controller]")]
    public class PersonalEventController : Controller
    {

        private readonly IGeneralProfileRepository _generalProfileRepository;
        private readonly IPersonalEventQueryService _personalEventQueryService;
        private readonly ICachSynchPersonalEventService _cachSynchPersonalEventService;
        private readonly IPersonalLocationQueryService _locationQueryService;
        private readonly IPartipationQueryService _partipationQueryService;

        public PersonalEventController(IGeneralProfileRepository personalRepository, IPersonalEventQueryService personalEventQueryService, ICachSynchPersonalEventService cachSynchPersonalEventService, IPersonalLocationQueryService locationQueryService, IPartipationQueryService partipationQueryService)
        {
            _generalProfileRepository = personalRepository;
            _personalEventQueryService = personalEventQueryService;
            _cachSynchPersonalEventService = cachSynchPersonalEventService;
            _locationQueryService = locationQueryService;
            _partipationQueryService = partipationQueryService;
        }

        /// <summary>
        /// Метод поиска событий для профиля на период от startTime до endTime. 
        /// Не указанная граница интервала принимает критическое значение со своей стороны.
        /// </summary>
        /// <param name="profId">Идентификатор профиля</param>
        /// <param name="startTime">Минимальное время для событий</param>
        /// <param name="endTime">Максимальное время для событий</param>
        /// <returns></returns>
        // GET: http://localhost:5230/api/PersonalEvent?profId=10000&startTime=1234265341&endTime=1234265347
        [HttpGet]
        public async Task<JsonResult> Get(int profId,long? startTime, long? endTime)
        {
            IEnumerable<string> events;

            if (DateConvert.IsTrueInterval(startTime, endTime)) events = await _personalEventQueryService.SearchEventsByIdProfile(profId, startTime.Value, endTime.Value);
                 else if (startTime == null && endTime == null) events = await _personalEventQueryService.SearchEventsByIdProfile(profId, -1, -1);
                    else if (endTime != null && endTime > 0) events = await _personalEventQueryService.SearchEventsByIdProfileStartNull(profId, endTime.Value);
                        else if (startTime != null && startTime > 0) events = await _personalEventQueryService.SearchEventsByIdProfileToEnd(profId, startTime.Value);
                            else
                            {
                                Response.BadRequest();
                                return null;
                            }
            if (events.Any()) return Json(events);
            Response.NotFoundRequest();
            return Json(Response.StatusCode); 
        }
        /// <summary>
        /// Выборка событий для профиля по Id и месяцу. Если месяц не указан, то возвращается для текущего.
        /// </summary>
        /// <param name="profId">Идентификатор профиля, который ищется по списку участников события. Если надо вычислить текущий месяц, то посылается -1</param>
        /// <param name="n">Номер месяца</param>
        /// <returns></returns>
        // GET: http://localhost:5230/api/PersonalEvent/m?profId=10000&n=-1
        [HttpGet("m")]
        public async Task<JsonResult> Get(int profId, int n)
        {

            var events = await _personalEventQueryService.SearchEventsByIdProfile(profId, n);
            if (events.Any()) return Json(events);
            Response.NotFoundRequest();
            return Json(Response.StatusCode);
        }
        /// <summary>
        /// Выборка события по Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<JsonResult> Get(int id)
        {
            //TODO Проверка на публичное событие. Не отдавать данные не участникам приватных событий.
            //TODO Проверка на участника события. Если участник - вернуть информацию независимо от типа события

            var data = await _personalEventQueryService.SearchEventAsync(id);
            if (data!=null)
            {
                return Json(data);
            }
            if (await _cachSynchPersonalEventService.SyncById(id))
            {
                var profilevent = await _personalEventQueryService.SearchEventAsync(id);
                if (profilevent != null) return Json(profilevent);
                //Ошибка работы с поисковым ботом.
                Response.ServerErrorRequest();
                return null;
            }
            Response.NotFoundRequest();
            return null;          
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="p">Флаг публикации изменений</param>
        /// <returns></returns>
        // POST api/values
        [HttpPost]
        public async Task Post([FromBody]PersonalEvent data, bool p)
        {
            if (data.NotExists())
            {
                Response.BadRequest();
                return;
            }
            //TODO Проверка, что пользователь - создатель события(айди из токена доступа)
            var domainObject= await _generalProfileRepository.GetEventAsync(PersonalEventId.Create(data.PersonalEventId.ToString()));
            if (domainObject==null)
            {
                Response.BadRequest();
                return;
            }
            try
            {
                domainObject.ChangeEventInformation(PersonalEventInformation.Create(data.Name, data.Description,
                     data.AvatarUrl))
                     .ChangeTimeOffset(data.TimeOffset)
                     .ChangeType((TypeEvent) data.Type)
                     .SwitchPeriodicity(data.IsPerodicity);
               var success = await _generalProfileRepository.UpdateAsync(domainObject, p);
               if (!success)
               {
                   Response.BadRequest();
               }

            }
            catch (Exception)
            {
                Response.BadRequest();
            }
     
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">Profile Id</param>
        /// <param name="data"></param>
        // PUT api/values/5
        [HttpPut("{id}")]
        public async Task Put(int id, [FromBody]PersonalEvent data)
        {
            if (data.NotExists())
            {
                Response.BadRequest();
                return;
            }
            var domainId = PersonalEventId.Create(data.PersonalEventId.ToString());
            var domainEvent = Domain.Entity.PersonalEvent.CreatePrivatePersonalEvent(domainId,
                LocationId.Create(data.PersonalLocationId.ToString()),
                data.TimeOffset, data.IsPerodicity,
                PersonalEventInformation.Create(data.Name, data.Description, data.AvatarUrl));

            var participation = Domain.Entity.Participation.CreateCreaterUnit(
                ParticipationId.Create(domainId,
                    GeneralProfileId.Create(id.ToString())));

            int eventId = await _generalProfileRepository.AddAsync(domainEvent, participation);

            if (eventId==-1)
            {
                Response.BadRequest();
                return;
            }
            var eventDto = GeneralProfileConvertService.ConvertToDto(domainEvent);
            eventDto.PersonalEventId = eventId;
            var loc = await _locationQueryService.GetPersonalLocationAsync(data.PersonalLocationId);
            var participations = await _partipationQueryService.GetParticipationsByIdEvent(eventId);
            bool success = await _cachSynchPersonalEventService.SyncData(eventDto, loc, participations);

        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            //TODO Проверка, что пользователь - создатель события(айди из токена доступа)

            if (!await _generalProfileRepository.DeleteAsync(PersonalEventId.Create(id.ToString())))
            {
                Response.BadRequest();
            }
          
        }
    }
}
