﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DailyBridge.Domain.Identity;
using DailyBridge.Domain.Interface.Enumeration;
using DailyBridge.Domain.Interface.Service;
using DailyBridge.Domain.Service;
using DailyBridge.Domain.ValueObject;
using DailyBridge.Infrastruction.Interface;
using DailyBridge.Utils;
using Data;
using Data.Extends;
using Microsoft.AspNet.Mvc;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace DailyBridge.Controllers
{
    [Route("api/[controller]")]
    public class PersonalNewsController : Controller
    {

        private readonly IGeneralProfileRepository _profilRepository;
        private readonly INewsQueryService _newsQueryService;
        private readonly ICachSyncPersonalNewsService _cachSyncPersonalNewsService;
        IGeneralProfileQueryService _generalProfileQueryService;

        public PersonalNewsController(IGeneralProfileRepository profilRepository, INewsQueryService newsQueryService, ICachSyncPersonalNewsService cachSyncPersonalNewsService, IGeneralProfileQueryService generalProfileQueryService)
        {
            _profilRepository = profilRepository;
            _newsQueryService = newsQueryService;
            _cachSyncPersonalNewsService = cachSyncPersonalNewsService;
            _generalProfileQueryService = generalProfileQueryService;
        }

        // GET: api/values
        [HttpGet]
        public IEnumerable<string> Get(int eventId, int numPage, int countProfileOnPage, int? acceptNews)
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }
        /// <summary>
        /// Ограничения:
        /// -Изменять новость события может только ее автор
        /// </summary>
        /// <param name="newDtoNews"></param>
        /// <returns></returns>
        // POST api/PersonalNews
        [HttpPost]
        public async Task Post([FromBody]News newDtoNews)
        {
            if (newDtoNews.NotExists())
            {
                Response.BadRequest();
                return;
            }
            var news = await _newsQueryService.GetNewsById(newDtoNews.NewsId);
            if (news.NotExists() || StatusNews.Checked == (StatusNews)news.Status)
            {
                Response.NotFoundRequest();
                return;
            }
            var oldDomainNews = GeneralProfileConvertService.ConvertToDomainOject(news);
            oldDomainNews.ChangeInformation(NewsInformation.Create(newDtoNews.Heading, newDtoNews.SubHeading,
                newDtoNews.Text))
                .ChangeTypeNotify((TypeNotify) newDtoNews.TypeNotify);

            //Ставим фейковый подходящий айди
            GeneralProfileId generalProfileId = GeneralProfileId.Create("10000");
            //
            if (!await _profilRepository.UpdateAuthorPrivilegesAsync(generalProfileId, oldDomainNews))
            {
                Response.BadRequest();
            }                
            
            //TODO синхронизация
            return;
        }

        /// <summary>
        /// Отобрение публикации новости.
        /// -Изменять статус новости может только создатель события
        /// </summary>
        /// <param name="id">Идентификатор новости</param>
        /// <returns></returns>
        [HttpPost("{id}")]
        public async Task Post(int id)
        {
            var news = await _newsQueryService.GetNewsById(id);
            if (news.NotExists() || StatusNews.Checked == (StatusNews)news.Status)
            {
                Response.NotFoundRequest();
                return;
            }
            var oldDomainNews = GeneralProfileConvertService.ConvertToDomainOject(news);
            oldDomainNews.ChangeStatus(StatusNews.Checked);
            //Ставим фейковый подходящий айди
            GeneralProfileId generalProfileId = GeneralProfileId.Create("10000");
            //
            if (!await _profilRepository.UpdateCreaterPrivilegesAsync(generalProfileId, oldDomainNews))
            {
                Response.BadRequest();
            }
            //TODO синхронизация c кэшем
            return;
        }
        // PUT api/values/5
        [HttpPut]
        public async Task Put([FromBody]News newsDto)
        {
            //TODO Взятие айди из токена доступа
            //Ставим фейковый подходящий айди
            GeneralProfileId generalProfileId = GeneralProfileId.Create("10000");
            //
            var domainNews = Domain.Entity.News.CreateNotCheckedNewsFakeId(
                PersonalEventId.Create(newsDto.PersonalEventId.ToString()), TypeNews.Info, (TypeNotify) newsDto.TypeNotify,
                NewsInformation.Create(newsDto.Heading, newsDto.SubHeading,newsDto.Text
                    ),GeneralProfileId.Create(newsDto.AuthorId.ToString()), newsDto.TimeOffset);
            if (domainNews==null)
            {
                Response.BadRequest();
                return;
            }
            if (!await _profilRepository.AddAsync(generalProfileId, domainNews))
            {
                Response.BadRequest();
            }

            var profile = await _generalProfileQueryService.GetProfileAsync(Convert.ToInt32(generalProfileId.Id), true);
            await _cachSyncPersonalNewsService.SyncData(newsDto, profile);
            //TODO синхронизация c кэшем
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
