﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DailyBridge.Infrastruction.Interface;
using DailyBridge.Infrastruction.Utils;
using Dapper;
using Data;

namespace DailyBridge.Infrastruction
{
    public class PersonalLocationQueryService : QueryService, IPersonalLocationQueryService
    {
        private string _simpleSearchQuery = @"{ 
                  ""fields"": [""GeneralProfileId"", ""PersonalLocationId"", ""Name"", ""Description"", ""Address""],
                  ""query"": { 
                      ""bool"": {                            
                           {2}

                         ""filter"": [
                              {3}
                           ]
                     }
               } }";
        private string _fuzzinesPart = @"""must"": [
                                {""multi_match"" : {
                            		  ""query"":      ""{2}"",
                                	  ""type"":       ""cross_fields"",
                             		  ""fields"":     [
                                 		   ""Name""
   		                                ]
}
                                }
                            ],";
        public PersonalLocationQueryService()
        {
            Type = "personal";
            Index = "location";
        }


        public async Task<IEnumerable<PersonalLocation>> SearchPersonalLocationsAsync(string text, string idProfile)
        {
            string body = _simpleSearchQuery;
            try
            {
                body = body.Replace("{2}", !string.IsNullOrEmpty(text) ? _fuzzinesPart.Replace("{2}", text) : " ");
                body = body.Replace("{3}", @"{ ""term"":  { ""GeneralProfileId"": {3} } }")
                        .Replace("{3}", idProfile);

                var res = await _client.SearchAsync<string>(body);
                return !res.Success ? null : ElasticSearchHelper.ConvertHitsToList<PersonalLocation>(res.Body);
            }
            catch (Exception e)
            {
                Console.WriteLine();
                throw;
            }
        }


        public async Task<PersonalLocation> SearchPersonalLocationAsync(string idLocation)
        {
            var pr = await _client.GetAsync<string>(Index, Type, idLocation);
            if (!pr.Success && pr.HttpStatusCode!=null) return null;
            if (pr.HttpStatusCode == null)
            {
                throw new IOException();
            }
            var prs = ElasticSearchHelper.ConvertHitToInstance<PersonalLocation>(pr.Body);
            return prs;
        }

        public async Task<IEnumerable<PersonalLocation>> GetPersonalLocationsAsync(string idProfile)
        {
            using (var dbConnection = GetOpenConnection())
            {
                try
                {
                    var loc = await dbConnection.QueryAsync<PersonalLocation>(
                   PQueryDictionary.LocationSelectQueryByProfile,
                    new { IdProf = idProfile });
                    return loc;
                }
                catch (Exception)
                {
                    return null;
                }

            }
        }

        public async Task<PersonalLocation> GetPersonalLocationAsync(int idLocation)
        {
            using (var dbConnection = GetOpenConnection())
            {
                try
                {
                    var loc = await dbConnection.QueryFirstOrDefaultAsync<PersonalLocation>(
                        PQueryDictionary.LocationSelectQueryById,
                    new { IdLoc = idLocation });
                    return loc;
                }
                catch (Exception)
                {
                    return null;
                }

            }
        }


    }
}
