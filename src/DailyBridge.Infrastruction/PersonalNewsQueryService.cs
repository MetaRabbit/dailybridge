﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DailyBridge.Infrastruction.Interface;
using DailyBridge.Infrastruction.Utils;
using Dapper;
using Data;

namespace DailyBridge.Infrastruction
{
    public class PersonalNewsQueryService : QueryService, INewsQueryService
    {
        public async Task<News> GetNewsById(int newsId)
        {

            using (db = GetOpenConnection())
            {
                try
                {
                    var news =
                         await db.QueryFirstOrDefaultAsync<News>(PQueryDictionary.NewsSelectQuery, new { NewsId = newsId });
                    return news;
                }
                catch (Exception)
                {
                    return null;
                }              
            }           
        }

        public Task<IEnumerable<News>> SearchNews(int eventId, int numPage, int countProfileOnPage, int? acceptNews)
        {
            throw new NotImplementedException();
        }
    }
}
