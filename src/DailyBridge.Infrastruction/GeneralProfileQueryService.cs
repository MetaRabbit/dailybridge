﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;
using DailyBridge.Infrastruction.Interface;
using DailyBridge.Infrastruction.Utils;
using Dapper;
using Data;
using Elasticsearch.Net;

namespace DailyBridge.Infrastruction
{
    public class GeneralProfileQueryService : QueryService, IGeneralProfileQueryService
    {

        private string _simpleSearchQuery = @"{  
                  ""from"" : {0}, ""size"" : {1},
                  ""fields"": [""GeneralProfileId"", ""FirstName"", ""LastName"", ""MiddleName"", ""AvatarUrl"", ""Status""],
                  ""query"": { 
                      ""bool"": { 
                           {2}
                          ""filter"": [
                              {3}
                           ]
                     }
               } }";
        private string _fuzzinesPart = @" ""must"": [
                                {""multi_match"" : {
                            		  ""query"":      ""{2}"",
                                	  ""type"":       ""cross_fields"",
                             		  ""fields"":     [
                                 		   ""FirstName"",
                                 		   ""LastName""
   		                                ]                                          
                                  }
                                }
                            ],";


        public GeneralProfileQueryService()
        {
            Index = "profile";
            Type = "generalprofile";
        }
        /// <summary>
        /// Выдает профиль пользователя. 
        /// isLite необходимо установить в true, если профиль запрашивает не его владелец.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isLite">Флаг выдачи укороченного/полного профиля</param>
        /// <returns></returns>
        public async Task<GeneralProfile> GetProfileAsync( int id, bool isLite )
        {
            var qResult  = await _client.GetAsync<string>( Index, Type, id.ToString() );
            if (!qResult .Success) return null;
            var profile = ElasticSearchHelper.ConvertHitToInstance<GeneralProfile>(qResult.Body);
            if (!isLite) return profile;
            profile.SetSecret();
            return profile;
        }

        public async Task<IEnumerable<GeneralProfile>> GetProfilesAsync(int[] ids)
        {
            using (var dbConnection = GetOpenConnection())
            {
                try
                {
                    var profiles = await
                        dbConnection.QueryAsync<GeneralProfile>(PQueryDictionary.ProfileSelectIncludeQuery,
                            new {ids = string.Join(",", ids)});
                    return profiles;
                }
                catch (Exception e)
                {

                    Console.WriteLine(e);
                }
                return null;
            }
           
        }

        public async Task<IEnumerable<GeneralProfile>> SearchProfilesAsync(int[] selectedUserIds, int numPage, int countProfileOnPage)
        {
            return await SearchProfilesAsync(null, selectedUserIds, numPage, countProfileOnPage);
        }

        public async Task<IEnumerable<GeneralProfile>> SearchProfilesAsync( string text, int[] selectedUserIds, int numPage,
              int countProfileOnPage )
        {
            string body = _simpleSearchQuery.Replace( "{0}",
                ElasticSearchHelper.PerformStartIndex( numPage, countProfileOnPage ).ToString() )
                .Replace( "{1}", countProfileOnPage.ToString() );

            try
            {
                body = body.Replace("{2}", !string.IsNullOrEmpty(text) ? _fuzzinesPart.Replace("{2}",text) : " ");

                if ( selectedUserIds != null && selectedUserIds.Length==0)
                {
                    body = body.Replace( "{3}", @"{ ""terms"":  { ""GeneralProfileId"": [{3}] } }" )
                        .Replace( "{3}", string.Join( ",", selectedUserIds ) );
                }
                else
                {
                    body = body.Replace( "{3}", " " );
                }
                
                var res = await _client.SearchAsync<string>( body );
                return ElasticSearchHelper.ConvertHitsToList<GeneralProfile>( res.Body ); 
               
            }
            catch (Exception e )
            {
                Console.WriteLine();
                throw;
            }
        }


    }
}
