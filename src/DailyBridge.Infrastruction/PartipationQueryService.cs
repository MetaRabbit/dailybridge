﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DailyBridge.Infrastruction.Interface;
using DailyBridge.Infrastruction.Utils;
using Dapper;
using Data;

namespace DailyBridge.Infrastruction
{
    public class PartipationQueryService : QueryService, IPartipationQueryService
    {

        public async Task<IEnumerable<Participation>> GetParticipationsByIdEvent(int eventId)
        {

            using (var dbConecction = GetOpenConnection())
            {
                var participations =await dbConecction.QueryAsync<Participation>( PQueryDictionary.ParticipationSelectQuery, new { PersonalEventId = eventId });
                return participations;

            }
        }

        public async Task<Participation> GetParticipation(int eventId, int profId)
        {

            using (var dbConecction = GetOpenConnection())
            {
                var participations = await dbConecction.QueryFirstOrDefaultAsync<Participation>(PQueryDictionary.ParticipationSelectQueryByProfile, new { PersonalEventId = eventId, GeneralProfileId = profId });
                return participations;

            }
        }
    }
}
