﻿
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Data;
using Npgsql;

namespace DailyBridge.Infrastruction.Extends
{
    public static class DataQueryExtends
    {
        public static async Task<bool> FastInsert(this PersonalLocation personalLocation)
        {
            
            using (var db = GetOpenConnection())
            {
                Console.WriteLine();


                var db1 = new NpgsqlConnection(@"Host=localhost;Database=personalDB;Username=postgres;Password=Rola1994");
                db1.Open();
                try
                {
                    await db.ExecuteAsync(@"insert into ""PersonalLocation"" values(@profId, @name,@desc,@address)",
                        new
                        {
                            personalLocation.GeneralProfileId,
                            personalLocation.Name,
                            personalLocation.Description,
                            personalLocation.Address
                        });

                    return true;
                }
                catch (Exception)
                {
                    return false;

                }

            }


        }

        private static DbConnection GetOpenConnection()
        {
            var db = new NpgsqlConnection(@"Host=localhost;Database=personalDB;Username=postgres;Password=Rola1994");
            db.Open();
            return db;
        }
    }
}
