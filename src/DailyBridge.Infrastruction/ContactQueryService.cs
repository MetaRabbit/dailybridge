﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DailyBridge.Infrastruction.Interface;
using DailyBridge.Infrastruction.Utils;
using Dapper;
using Data;

namespace DailyBridge.Infrastruction
{
    public class ContactQueryService : QueryService, IContactQueryService
    {
        private string _simpleSearchQuery = @"{  
            {0}
                  ""query"": { 
                      ""bool"": {                          
                          ""filter"": [
                                { ""term"":  { ""Source"": ""{2}"" }}                            
                           ]
                     }
               } }";
        /// <summary>
        /// Выборка контактов из базы данных. 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status">Статус контакта. 0 - ActiveOnFrontEnd, 1 - Active, 2 - Blocked</param>
        /// <param name="pageNumber">Номер страницы.</param>
        /// <param name="contactOnPage"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Contact>> GetContactsListAsync( int id, int status, int pageNumber, int contactOnPage)
        {
            //TODO typesContact Parse To Indexes
            using (var dbConnection = GetOpenConnection())
            {
                var pro = await dbConnection.QueryAsync<Contact>( PQueryDictionary.ContactSelectPagingQuery,
                    new { UserId = id, Type = status, Limit = contactOnPage, Offset = ElasticSearchHelper.PerformStartIndex(pageNumber, contactOnPage) } );
                return pro;
            }
           
        }

        public async Task<IEnumerable<Contact>> GetContactsListAsync(int id, int status)
        {
            using (var dbConnection = GetOpenConnection())
            {
                var pro = await dbConnection.QueryAsync<Contact>(PQueryDictionary.ContactSelectQuery,
                    new { UserId = id, Type = status});
                return pro;
            }
        }

        public async Task<IEnumerable<Contact>> SearchContactsListAsync(int id, int numPage, int countContactOnPage)
        {
            string body = _simpleSearchQuery
                .Replace("{0}", @"""from"" : {0}, ""size"" : {1},")                
                .Replace( "{0}",
                ElasticSearchHelper.PerformStartIndex( numPage, countContactOnPage ).ToString() )
                .Replace( "{1}", countContactOnPage.ToString() )
                .Replace( "{2}", id.ToString() );

            var res = await _client.SearchAsync<string>("contact", "list", body);
            return ElasticSearchHelper.ConvertHitsToList<Contact>( res.Body );
        }

        public async Task<IEnumerable<Contact>> SearchContactsListAsync(int id)
        {
            string body = _simpleSearchQuery
               .Replace("{0}", "")
               .Replace("{2}", id.ToString());

            var res = await _client.SearchAsync<string>("contact", "list", body);
            return ElasticSearchHelper.ConvertHitsToList<Contact>(res.Body);
        }
    }
}
