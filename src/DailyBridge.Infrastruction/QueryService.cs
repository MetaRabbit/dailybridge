﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Runtime.CompilerServices;
using Dapper;
using Elasticsearch.Net;
using Nest;
using Npgsql;

namespace DailyBridge.Infrastruction
{
    public class QueryService
    {
        public static string ConnectionString =
            @"Host=localhost;Database=PersonalDb;Username=postgres;Password=Rola1994";

        protected DbConnection db;
        protected ElasticLowLevelClient _client;
        protected string Index;
        protected string Type;

        protected QueryService()
        {
            var node = new Uri("http://localhost:9200");

            var settings = new ConnectionSettings(node);
            _client = new ElasticLowLevelClient(settings);
       
        }
        protected DbConnection GetOpenConnection()
        {
            db = new NpgsqlConnection(ConnectionString);
            db.Open();
            return db;
        }

    }
}
