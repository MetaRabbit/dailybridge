﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DailyBridge.Infrastruction.Interface;
using Dapper;
using Data;
using Elasticsearch.Net;
using Nest;
using Newtonsoft.Json;

namespace DailyBridge.Infrastruction.Synchronizer
{
    public class CacheSynchContactService : QueryService, ICacheSynchContactService
    {
        private IContactQueryService _contactQuaryService;
        private string _bulkQueryFirstPart = @"{""index"":{""_index"":""contact"",""_type"":""list"",""_id"": ""{0}""}}";
        private string _bulkQuerySource = @"{""Target"": {1}, ""Source"":{2}}";
        private static readonly int FrontEndContactEnumIndex = 0;

        public CacheSynchContactService(IContactQueryService contactQuaryService)
        {
            _contactQuaryService = contactQuaryService;
            Index = "contact";
            Type = "list";

        }

        public async Task<bool> SyncFrondEndContacts(int profileId)
        {
            using (var db = GetOpenConnection())
            {
                var contacts =
                    await _contactQuaryService.GetContactsListAsync(profileId, FrontEndContactEnumIndex);
                if (!contacts.Any())
                {
                    return true;
                }
            
            List<string> listBulkItems = new List<string>();
            foreach (var newContact in contacts)
            {
                listBulkItems.Add(_bulkQueryFirstPart.Replace("{0}", newContact.Target + "" + newContact.Source));
                listBulkItems.Add(_bulkQuerySource.Replace("{1}", newContact.Target.ToString())
                    .Replace("{2}", newContact.Source.ToString()));
            }
            var responseBulk =
                await _client.BulkPutAsync<string>("contact", "list", new PostData<object>(listBulkItems));
            //TODO CHECK RESPONSE
            Console.WriteLine(responseBulk);
            return true;
        }
    }

        public async Task<bool> SyncContact(string idSource, string idTarget, int status)
        {
            var contactSearch = await _client.GetAsync<Contact>(Index, Type, idTarget+ ""+ idSource );
            if (status.Equals(FrontEndContactEnumIndex))
            {
                if (!contactSearch.Success)
                {
                    await _client.IndexAsync<Contact>(Index, Type, idTarget + "" + idSource,JsonConvert.SerializeObject((new Contact() { Target = Convert.ToInt32(idTarget) ,Source = Convert.ToInt32(idSource)})));
                }
                return true;
            }
            await _client.DeleteAsync<Contact>(Index, Type, idTarget + "" + idSource);
            return true;
        }
    }
}
