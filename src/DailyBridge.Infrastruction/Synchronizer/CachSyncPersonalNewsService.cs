﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyBridge.Infrastruction.Interface;
using DailyBridge.Infrastruction.Utils;
using Dapper;
using Data;
using Elasticsearch.Net;
using Newtonsoft.Json;

namespace DailyBridge.Infrastruction.Synchronizer
{
    public class CachSyncPersonalNewsService : QueryService, ICachSyncPersonalNewsService
    {

        private string _bulkQueryFirstPart = @"{""index"":{""_index"":""news"",""_type"":""personal"",""_id"": ""{0}""}}";
        private string _bulkQueryLastPart = @"{""NewsId"": {1}, ""PersonalEventId"":{2}, ""TypeNotify"":{3}, ""Type"":{4}, ""Text"":{5},
                                                                    ""TimeOffset"":{6}, ""SubHeading"":{7}, ""Heading"":{8}, ""AuthorId"":{9}
                                                                           ,""AuthorName"":{10}, ""Status"":{11}}";

        public CachSyncPersonalNewsService()
        {
            Index = "news";
            Type = "personal";
        }

        public async Task SyncData(News news, GeneralProfile generalProfile)
        {
            var esNews = new
            {
                NewsId = news.NewsId,
                PersonalEventId = news.PersonalEventId,
                TypeNotify = news.TypeNotify,
                Type = news.Type,
                Text = news.Text,
                TimeOffset = news.TimeOffset,
                SubHeading = news.SubHeading ?? "",
                Heading = news.Heading,
                AuthorId = news.AuthorId,
                AuthorName = generalProfile.FirstName + " " + generalProfile.LastName,
                Status = news.Status
            };
            try
            {
                await _client.IndexAsync<string>(Index, Type, JsonConvert.SerializeObject(esNews));
            }
            catch (Exception e)
            {
                Console.WriteLine();
                throw;
            }
           
        }

        public async Task<bool> SyncDb(int newsId)
        {
            using (db = GetOpenConnection())
            {
                try
                {
                    var news = await db.QueryAsync<News>(PQueryDictionary.NewsSelectQuery, new { NewsId = newsId });
                    if (!news.Any())
                    {
                        return true;
                    }
                    var profilesIds = news.Select(news1 => news1.AuthorId);
                    var profiles = await db.QueryAsync<GeneralProfile>(PQueryDictionary.ProfileSelectIncludeQuery, new { ids = string.Join(",", profilesIds) });

                    List<string> listBulkItems = new List<string>();
                    StringBuilder partBulkData = new StringBuilder();
                    foreach (var newNews in news)
                    {
                        var profile = profiles.FirstOrDefault(profile1 => profile1.GeneralProfileId == newNews.AuthorId);                                             
                        listBulkItems.Add(_bulkQueryFirstPart.Replace("{0}", newNews.NewsId.ToString()));

                        partBulkData.AppendFormat(_bulkQueryLastPart, newNews.NewsId,
                            newNews.PersonalEventId, newNews.TypeNotify, newNews.Type,
                            newNews.Text, newNews.TimeOffset, newNews.SubHeading, newNews.Heading,
                            newNews.AuthorId, profile.FirstName + " " + profile.LastName,
                            newNews.Status);

                        listBulkItems.Add(partBulkData.ToString());
                        partBulkData.Clear();
                    }
                    var responseBulk =
                        await _client.BulkPutAsync<string>(Index, Type, new PostData<object>(listBulkItems));
                    if (!responseBulk.Success)
                    {
                        return false;
                        //TODO записать в лог
                    }
                    return true;
                }
                catch (Exception)
                {
                    return false;
                    throw;
                }
              
            }
        }
    }
}
