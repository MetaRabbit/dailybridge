﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DailyBridge.Infrastruction.Interface;
using Data;
using Newtonsoft.Json;

namespace DailyBridge.Infrastruction.Synchronizer
{
    public class CachSynchPersonalEventService : QueryService, ICachSynchPersonalEventService
    {
        IPersonalEventQueryService _personalEventQueryService;
        IPersonalLocationQueryService _personalLocationQueryService;
        IPartipationQueryService _partipationQueryService;
       

        public CachSynchPersonalEventService(IPersonalEventQueryService personalEventQueryService, IPersonalLocationQueryService personalLocationQueryService, IPartipationQueryService partipationQueryService)
        {
            _personalEventQueryService = personalEventQueryService;
            _personalLocationQueryService = personalLocationQueryService;
            _partipationQueryService = partipationQueryService;
            Index = "event";
            Type = "personal";
        }

        public async Task<bool> SyncById(int id)
        {
            var personalEvent=  await _personalEventQueryService.GetEventAsync(id);
            var location = await _personalLocationQueryService.GetPersonalLocationAsync(personalEvent.PersonalLocationId);
            var participations = await _partipationQueryService.GetParticipationsByIdEvent(id);

            return await SyncData(personalEvent, location, participations);
        }

        public async Task<bool> SyncData(PersonalEvent personalEvent, PersonalLocation location, IEnumerable<Participation> participations)
        {

                var eSparticipations =
                    participations.Select(
                        participation =>
                            new {GeneralProfileId = participation.GeneralProfileId, Type = participation.Type});
                var esEvent = new
                {
                    PersonalEventId = personalEvent.PersonalEventId,
                    TimeOffset = personalEvent.TimeOffset,
                    IsPerodicity = personalEvent.IsPerodicity,
                    Description = personalEvent.Description,
                    Name = personalEvent.Name,
                    AvatarUrl = personalEvent.AvatarUrl,
                    Type = personalEvent.Type,
                    Location = new { Name = location.Name, Description = location.Description, Address = location.Address },
                    Participation = eSparticipations 

            };

            var response =
                await
                    _client.IndexAsync<string>(Index, Type, esEvent.PersonalEventId.ToString(),
                        JsonConvert.SerializeObject(esEvent));

            Console.WriteLine(response);
            return true;
        }

        public Task<bool> SyncOnMonthByProfileId(int id)
        {
            throw new NotImplementedException();
        }
    }
}
