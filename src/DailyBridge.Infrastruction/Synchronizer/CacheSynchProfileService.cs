﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DailyBridge.Infrastruction.Interface;
using DailyBridge.Infrastruction.Utils;
using Dapper;
using Data;
using Elasticsearch.Net;
using Newtonsoft.Json;

namespace DailyBridge.Infrastruction.Synchronizer
{
    public class CacheSynchProfileService : QueryService, ICacheSynchProfileService
    {

        private IContactQueryService _contactQuaryService;
        private ICacheSynchContactService _cacheSynchContactService;

        public CacheSynchProfileService(IContactQueryService contactQuaryService, ICacheSynchContactService cacheSynchContactService)
        {
            _contactQuaryService = contactQuaryService;
            _cacheSynchContactService = cacheSynchContactService;
            Index = "profile";
            Type = "generalprofile";
        }

        public void SyncData(GeneralProfile profile)
        {
            //_client.PutAsync(new IndexCommand("dailybridge", "profile", profile.GeneralProfileId.ToString()),
            //   _serializer.Serialize(profile));
            var response = _client.Index<string>(Index, Type, profile.GeneralProfileId.ToString(), JsonConvert.SerializeObject(profile));
            Console.WriteLine(response);
        }

        /// <summary>
        /// Try to put profile in cach from DB
        /// </summary>
        /// <param name="id">Profile`s id</param>
        /// <param name="isLite"></param>
        /// <returns>true, if DB constrains profile</returns>
        public async Task<bool> TrySyncDbAsync(int id, bool isLite)
        {
            //TODO set ttl at 1 mounth
            //sync profile
            using (var db = GetOpenConnection())
            {
                var profile =
                          db.QueryFirstOrDefaultAsync<GeneralProfile>(
                                PQueryDictionary.ProfileSelectQuery, new { UserId = id }).Result;
                if (profile == null)
                {
                    return false;
                }
                var response = _client.Index<string>(Index, Type, profile.GeneralProfileId.ToString(), JsonConvert.SerializeObject(profile));
                Console.WriteLine(response);
            }


   
            if (isLite)
            {
                return true;
            }
            bool status = await _cacheSynchContactService.SyncFrondEndContacts(id);
            //TODO CHECK

            return true;
        }

    }
}
