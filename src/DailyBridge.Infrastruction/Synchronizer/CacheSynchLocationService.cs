﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DailyBridge.Infrastruction.Interface;
using Dapper;
using Data;
using Elasticsearch.Net;
using Newtonsoft.Json;

namespace DailyBridge.Infrastruction.Synchronizer
{
    public class CacheSynchLocationService : QueryService, ICachSynchLocationService
    {
        IPersonalLocationQueryService _personalLocationQueryService;
        private string _bulkQueryFirstPart = @"{""index"":{""_index"":""contact"",""_type"":""list"",""_id"": ""{0}""}}";
        private string _bulkQuerySource = @"{""GeneralProfileId"" : {1}, ""PersonalLocationId"" : {2}, ""Name"" : {3}, ""Description"" : {4}, ""Address"" : {5}}";

        public CacheSynchLocationService(IPersonalLocationQueryService personalLocationQueryService)
        {
            _personalLocationQueryService = personalLocationQueryService;
            Index = "location";
            Type = "personal";
        }

        public async Task<bool> SyncLocationsByProfileId(string profileId)
        {
            var locations = await _personalLocationQueryService.GetPersonalLocationsAsync(profileId);

            if ((locations != null && !locations.Any()) || locations ==null)
            {
                //Удаление единственного элемента из поискового движка
                var locastions = await _personalLocationQueryService.SearchPersonalLocationsAsync(null, profileId);

                await _client.DeleteAsync<PersonalLocation>(Index, Type,
                    locastions.FirstOrDefault().PersonalLocationId.ToString());
                return true;
            }

            List<string> listBulkItems = new List<string>();
            foreach (var location in locations)
            {
                listBulkItems.Add(_bulkQueryFirstPart.Replace("{0}", location.PersonalLocationId.ToString()));
                listBulkItems.Add(_bulkQuerySource.Replace("{1}", location.GeneralProfileId.ToString())
                    .Replace("{2}", location.PersonalLocationId.ToString())
                    .Replace("{3}", location.Name)
                    .Replace("{4}", location.Description)
                    .Replace("{5}", location.Address));
            }

            var responseBulk =
              await _client.BulkPutAsync<string>("contact", "list", new PostData<object>(listBulkItems));
            //TODO CHECK RESPONSE
            Console.WriteLine(responseBulk);
            return true;
        }

        public async Task<bool> SyncLocationById(int locId)
        {
            var location = await _personalLocationQueryService.GetPersonalLocationAsync(locId);
            var response = await _client.IndexAsync<PersonalLocation>(Index, Type, locId.ToString(), JsonConvert.SerializeObject(location));
            //TODO CHECK RESPONSE
            Console.WriteLine(response);
            return true;
        }
    }
}
