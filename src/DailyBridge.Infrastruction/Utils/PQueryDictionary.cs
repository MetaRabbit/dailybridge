﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data;

namespace DailyBridge.Infrastruction.Utils
{
    public class PQueryDictionary
    {
        #region Profile Query
        /// <summary>
        /// GeneralProfileId = @UserId;
        /// </summary>
        public static string ProfileSelectQuery =
            @"SELECT * FROM ""GeneralProfile"" WHERE ""GeneralProfileId"" = @UserId";
        /// <summary>
        /// GeneralProfileId in (@ids)"
        /// </summary>
        public static string ProfileSelectIncludeQuery =
           @"SELECT * FROM ""GeneralProfile"" WHERE ""GeneralProfileId"" in (@ids)";

        #endregion

        #region Contact Query
        /// <summary>
        /// Status = @Type AND Source = @UserId
        /// </summary>
        public static string ContactSelectQuery =
            @"SELECT * FROM ""Contact"" WHERE ""Status"" = @Type AND ""Source"" = @UserId";
        /// <summary>
        /// Status = @Type AND Source = @UserId LIMIT @Limit OFFSET @Offset
        /// </summary>
        public static string ContactSelectPagingQuery =
            @"SELECT * FROM ""Contact"" WHERE ""Source"" = @UserId AND ""Status"" = @Type LIMIT @Limit OFFSET @Offset";
        #endregion

        #region Event Query
        /// <summary>
        /// PersonalEventId = @eventId
        /// </summary>
        public static string EventSelectQuery =
             @"select * from ""PersonalEvent"" where ""PersonalEventId"" = @eventId";
        #endregion

        #region Location Query
        /// <summary>
        /// GeneralProfileId = @IdProf
        /// </summary>
        public static string LocationSelectQueryByProfile =
              @"select * from ""PersonalLocation"" where ""GeneralProfileId"" = @IdProf";
        /// <summary>
        /// PersonalLocationId = @IdLoc
        /// </summary>
        public static string LocationSelectQueryById =
             @"select * from ""PersonalLocation"" where ""PersonalLocationId"" = @IdLoc ";
        #endregion

        #region Participation
        /// <summary>
        /// PersonalEventId = @PersonalEventId
        /// </summary>
        public static string ParticipationSelectQuery =  @"select * from ""Participation"" where ""PersonalEventId""= @PersonalEventId";
        /// <summary>
        /// PersonalEventId = @PersonalEventId, GeneralProfileId= @GeneralProfileId
        /// </summary>
        public static string ParticipationSelectQueryByProfile = @"select * from ""Participation"" where ""PersonalEventId""= @PersonalEventId AND ""GeneralProfileId""= @GeneralProfileId";
        #endregion

        #region News 
        /// <summary>
        /// NewsId= @NewsId
        /// </summary>
        public static string NewsSelectQuery = @"select * from ""News"" where ""NewsId""= @NewsId";

        #endregion
    }
}
