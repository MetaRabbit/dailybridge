﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Data;
using Npgsql;

namespace DailyBridge.Infrastruction.Utils
{
    public static class FastInsertExtends
    {
        public static async Task<bool> FastInsertAsync(this IPersonalRepository personalRepository, PersonalLocation personalLocation)
        {

            using (var db = GetOpenConnection())
            {
                try
                {
                    await db.ExecuteAsync(@"insert into ""PersonalLocation"" ( ""GeneralProfileId"", ""Name"", ""Description"", 
            ""Address"") values( @profId, @name,@desc,@address)",
                        new
                        {
                            profId =personalLocation.GeneralProfileId,
                            name=personalLocation.Name,
                            desc=personalLocation.Description,
                            address=personalLocation.Address
                        });

                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        private static DbConnection GetOpenConnection()
        {
            var db = new NpgsqlConnection(QueryService.ConnectionString);
            db.Open();
            return db;
        }

        public static async Task<int> FastInsertAsync(this IPersonalRepository personalRepository, PersonalEvent personalEvent, Participation participation)
        {
            using (var db = GetOpenConnection())
            {
                var dbTransaction = db.BeginTransaction(IsolationLevel.ReadCommitted);
                try
                {
                 
                     var id = db.Query<int>(@"
                            insert into ""PersonalEvent"" (
                             ""PersonalLocationId"", ""TimeOffset"", ""IsPerodicity"",""Name"", ""Description"", ""AvatarUrl"", ""Type"") 
                                 values( @PersonalLocationId, @TimeOffset,@IsPerodicity,@Name, @Description, @AvatarUrl, @Type ) RETURNING ""PersonalEventId"";
                        ",
                        new
                        {
                            PersonalLocationId = personalEvent.PersonalLocationId,
                            Name = personalEvent.Name,
                            Description = personalEvent.Description,
                            TimeOffset = personalEvent.TimeOffset,
                            IsPerodicity = personalEvent.IsPerodicity,
                            AvatarUrl = personalEvent.AvatarUrl,
                            Type = personalEvent.Type
                        }, dbTransaction).Single();

                    await db.ExecuteAsync(@"insert into ""Participation"" (
                        ""GeneralProfileId"", ""PersonalEventId"", ""Type"", ""Comment"") 
                            values( @GeneralProfileId, @PersonalEventId,@Type,@Comment)",
                         new
                         {
                             GeneralProfileId = participation.GeneralProfileId,
                             PersonalEventId = id,
                             Type = participation.Type,
                             Comment = participation.Comment
                         }, dbTransaction);

                    dbTransaction.Commit();
                    return id;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    dbTransaction.Rollback();
                    return -1;
                }
                
            }
        }


        public static async Task<bool> FastInsertAsync(this IPersonalRepository personalRepository, News news)
        {

            using (var db = GetOpenConnection())
            {
                try
                {



                    await db.ExecuteAsync(@"insert into ""News"" ( ""PersonalEventId"", ""Type"", ""TypeNotify"", 
            ""Text"", ""TimeOffset"", ""Heading"",""SubHeading"",""AuthorId"", ""Status"") values( @PersonalEventId, @Type,@TypeNotify,@Text,@TimeOffset, @Heading,@SubHeading, @AuthorId, @Status)",
                        new
                        {
                            PersonalEventId = news.PersonalEventId,
                            Type = news.Type,
                            TypeNotify = news.TypeNotify,
                            Text = news.Text,
                            TimeOffset = news.TimeOffset,
                            Heading = news.Heading,
                            SubHeading = news.SubHeading,
                            AuthorId = news.AuthorId,
                            Status = news.Status
                        });



                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }
    }
}
