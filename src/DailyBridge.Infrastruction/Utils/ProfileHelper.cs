﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data;
using Newtonsoft.Json.Linq;

namespace DailyBridge.Infrastruction.Utils
{
    public static class ProfileHelper
    {
        public static void SetSecret(this GeneralProfile generalProfile)
        {
            generalProfile.PhoneNumber = "";
            generalProfile.Email = "";
        }
    }
}
