﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Data;
using Npgsql;

namespace DailyBridge.Infrastruction.Utils
{
    public static class FastDeleteExtends
    {
        public static async Task<bool> FastDeletePersonalLocationAsync(this IPersonalRepository personalRepository, int personalLocId, int profId)
        {

            using (var db = GetOpenConnection())
            {
                try
                {
                    await db.ExecuteAsync(@"delete from ""PersonalLocation"" where ""PersonalLocationId""= @locId AND @ProfId = ""GeneralProfileId"" ",
                        new
                        {
                            locId = personalLocId,
                            ProfId = profId
                        });

                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static async Task<bool> FastDeletePersonalEventAsync(this IPersonalRepository personalRepository, int personalEvent)
        {

            using (var db = GetOpenConnection())
            {
                try
                {
                    await db.ExecuteAsync(@"delete from ""PersonalEvent"" where ""PersonalEventId""= @eventId ",
                        new
                        {
                            eventId = personalEvent
                        });

                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        private static DbConnection GetOpenConnection()
        {


            var db = new NpgsqlConnection(QueryService.ConnectionString);
            db.Open();
            return db;
        }
    }
}
