﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Data;
using Npgsql;

namespace DailyBridge.Infrastruction.Utils
{
    public static class FastUpdateExtends
    {
        public static async Task<bool> FastUpdateAsync(this IPersonalRepository personalRepository,  PersonalLocation personalLocation)
        {

            using (var db = GetOpenConnection())
            {

                try
                {
                    await db.ExecuteAsync(@"update ""PersonalLocation"" set ""Name"" = @name, ""Description""= @desc, 
                        ""Addres""= @address where ""PersonalLocationId""= @locId",
                        new
                        {
                            locId = personalLocation.PersonalLocationId,
                            name = personalLocation.Name,
                            desc = personalLocation.Description,
                            address = personalLocation.Address
                        });

                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static async Task<bool> FastUpdateAsync(this IPersonalRepository personalRepository, PersonalEvent personalEvent, News news)
        {

            using (var db = GetOpenConnection())
            {
                var dbTransaction = db.BeginTransaction(IsolationLevel.Serializable);
                try
                {
                    await db.ExecuteAsync(@"update ""PersonalEvent"" set ""TimeOffset"" = @TimeOffset, ""IsPerodicity""= @IsPeriodicity, 
                        ""Name""= @Name, ""Description""= @Description, ""AvatarUrl"" = @AvatarUrl, ""Type"" = @Type where ""PersonalEventId""=@id",
                        new
                        {
                            TimeOffset = personalEvent.TimeOffset,
                            IsPeriodicity = personalEvent.IsPerodicity,
                            Name = personalEvent.Name,
                            Description = personalEvent.Description,
                            AvatarUrl = personalEvent.AvatarUrl,
                            Type = personalEvent.Type,
                            id=personalEvent.PersonalEventId
                        }, dbTransaction);

                    if (news!=null)
                    {
                        await db.ExecuteAsync(@"insert into ""News"" ( ""PersonalEventId"", ""Type"", ""TypeNotify"", 
                          ""Text"", ""TimeOffset"") values( @PersonalEventId, @Type, @TypeNotify, @Text, @TimeOffset)",
                                   new
                                   {
                                       PersonalEventId = news.PersonalEventId,
                                       Type = news.Type,
                                       TypeNotify = news.TypeNotify,
                                       Text = news.Text,
                                       TimeOffset = news.TimeOffset
                                   }, dbTransaction);
                    }


                    dbTransaction.Commit();
                    return true;
                }
                catch (Exception exception)
                {
                    dbTransaction.Rollback();
                    return false;
                }
            }
        }

        public static async Task<bool> FastUpdateAsync(this IPersonalRepository personalRepository, News news, int authorId)
        {

            using (var db = GetOpenConnection())
            {

                try
                {
                    await db.ExecuteAsync(@"update ""News"" set ""TypeNotify""= @TypeNotify, 
                        ""Text""= @Text,""Heading""= @Heading,""SubHeading""= @SubHeading,""Status""= @Status
                                where ""NewsId""= @NewsId AND ""PersonalEventId""= @PersonalEventId AND ""AuthorId""= @authorId ",
                        new
                        {
                            NewsId = news.NewsId,
                            PersonalEventId = news.PersonalEventId,
                            AuthorId = news.AuthorId,
                            TypeNotify = news.TypeNotify,
                            Text = news.Text,
                            Heading = news.Heading,
                            SubHeading = news.SubHeading,
                            Status = news.Status
                        });

                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }



        private static DbConnection GetOpenConnection()
        {
            var db = new NpgsqlConnection(QueryService.ConnectionString);
            db.Open();
            return db;
        }
    }
}

