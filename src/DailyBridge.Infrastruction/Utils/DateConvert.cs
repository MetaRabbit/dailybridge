﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DailyBridge.Infrastruction.Utils
{
    public class DateConvert
    {


        public static DateTime ConvertFromUnixTimestamp(double timestamp)
        {

            var origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);

            return origin.AddSeconds(timestamp);

        }

        public static long ToUnixTimespan(DateTime date)
        {
            TimeSpan tspan = date.ToUniversalTime().Subtract(
               new DateTime(1970, 1, 1, 0, 0, 0));

            return (long)Math.Truncate(tspan.TotalSeconds);
        }

        public static long[] GetInterval(int mounth)
        {
            var year = DateTime.UtcNow.Year;

            var startTime = ToUnixTimespan(new DateTime(year, mounth, 1));
            var endTime = ToUnixTimespan(new DateTime(year, mounth, DateTime.DaysInMonth(year, mounth)));

            return new[] {startTime, endTime};

        }

        public static bool IsTrueInterval(long? startTime, long? endTime)
        {
            return endTime != null && endTime > 0 && startTime != null && startTime > 0;

        }
    }
}
