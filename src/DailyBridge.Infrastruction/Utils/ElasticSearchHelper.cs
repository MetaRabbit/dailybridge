﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Data;
using Newtonsoft.Json.Linq;


namespace DailyBridge.Infrastruction.Utils
{
    public static class ElasticSearchHelper
    {
        
        public static List<T> ConvertHitsToList<T>(string json)
        {
            JObject jObject = JObject.Parse(json);
            var res = jObject["hits"]["hits"].Select(obj => obj["fields"]);
            List<T> listResult = new List<T>();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("{");
            foreach (var re in res)
            {
                var list = re.ToList();
                for (int i = 0; i < list.Count; i++)
                {
                    var lastName = list[i].Path.Split('.').Last();
                    string temp = @"""" + typeof(T).GetProperty(lastName).Name + @"""";
                    string temp2 = list[i].First[0].ToString();
                    if (i==list.Count)
                    {
                        stringBuilder.Append(temp + ":" + @""""+ temp2 + @"""");
                    }
                    else
                    {
                        stringBuilder.Append(temp + ":" + @"""" + temp2 + @"""" + ",");
                    }
                    
                }
                stringBuilder.Append("}");
                listResult.Add(JObject.Parse(stringBuilder.ToString()).ToObject<T>());
                stringBuilder.Clear();
                stringBuilder.Append("{");
            }

            return listResult;

        }

        public static List<string> ConvertHitsToJsonList(string json)
        {
            JObject jObject = JObject.Parse(json);
            return jObject["hits"]["hits"].Select(obj => obj["_source"].ToString()).ToList();

        }



        public static T ConvertHitToInstance<T>(string json)
        {
            JObject jObject = JObject.Parse(json);
            return jObject["_source"].ToObject<T>();
        }

        public static string ConvertHitToJsonInstance(string json)
        {
            JObject jObject = JObject.Parse(json);
            var res = jObject["hits"]["hits"].Select(obj => obj["fields"]);
            return res.ToString();

        }

        public static string ConvertHitToJsonInstanceSource(string json)
        {
            JObject jObject = JObject.Parse(json);
            var res = jObject["hits"]["hits"].Select(obj => obj["_source"].ToString()).FirstOrDefault();
            return res;

        }

        public static int PerformStartIndex(int numPage, int countObjectOnPage)
        {
            if (numPage == 0)
            {
                return 0;
            }
            return numPage*countObjectOnPage - countObjectOnPage;
        }


    }
}
