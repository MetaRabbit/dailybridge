﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DailyBridge.Infrastruction.Interface;
using DailyBridge.Infrastruction.Utils;
using Dapper;
using Data;

namespace DailyBridge.Infrastruction
{
    public class PersonalEventQueryService : QueryService, IPersonalEventQueryService
    {
        private string _simpleSearchQuery = @"{                
                  ""query"": { 
                      ""bool"": {                            
                          

                         ""filter"": [
                              {1}
                              {2}
                           ]
                     }
               } }";

        private string _filerRange = @"{""range"" : {
        ""TimeOffset"" : {
            {1}
            {2}
            ""boost"" : {3}
        }
         }}
        ";


        public PersonalEventQueryService()
        {
            Index = "event";
            Type = "personal";
        }

        public async Task<PersonalEvent> GetEventAsync(int id)
        {
            using (var dbConnection = GetOpenConnection())
            {
                var personalEvent =await dbConnection.QueryFirstOrDefaultAsync<PersonalEvent>(
                    PQueryDictionary.EventSelectQuery, new {eventId = id});
                return  personalEvent;
            }
        }

        public async Task<string> SearchEventAsync(int id)
        {
            string body = _simpleSearchQuery;

            body = body.Replace("{1}", @"{ ""term"":  { ""PersonalEventId"": {1} } }")
                       .Replace("{1}", id.ToString());
            body = body.Replace("{2}", "");
            var res = await _client.SearchAsync<string>(Index, Type, body);
            return !res.Success || res.HttpStatusCode==404 ? null : ElasticSearchHelper.ConvertHitToJsonInstanceSource(res.Body);
        }

        public async Task<IEnumerable<string>> SearchEventsByIdProfile(int idprofile, long start, long end)
        {
            string body = _simpleSearchQuery;

            body = body.Replace("{1}", @"{ ""term"":  { ""Participation.GeneralProfileId"": {1} } }")
                       .Replace("{1}", idprofile.ToString());

            if (start != -1 || end !=-1)
            {
                body = body.Replace("{2}", _filerRange);
            }

            body = start != -1 ? body.Replace("{1}", @",""gte"" : {4},").Replace("{4}", start.ToString()) : body.Replace("{1}", "");
            body = end != -1 ? body.Replace("{2}", @"""lte"" : {2},").Replace("{2}", end.ToString()) : body.Replace("{2}", "");
            if (start != -1 && end != -1)
            {
                body = body.Replace("{3}", "2.0");
            }
            else
            {
                body = body.Replace("{3}", "1.0");           
            }

            var res = await _client.SearchAsync<string>(body);
            return !res.Success || res.HttpStatusCode == 404 ? null : ElasticSearchHelper.ConvertHitsToJsonList(res.Body);

        }

        public async Task<IEnumerable<string>> SearchEventsByIdProfileToEnd(int idprofile, long start)
        {
           return await SearchEventsByIdProfile(idprofile, start, -1);
        }

        public async Task<IEnumerable<string>> SearchEventsByIdProfileStartNull(int idprofile, long end)
        {
            return await SearchEventsByIdProfile(idprofile, -1, end);
        }

        public async Task<IEnumerable<string>> SearchEventsByIdProfile(int idprofile, int month)
        {
            if (month == -1) month = DateTime.UtcNow.Month;
            if ( month < 1 || month > 12)
            {
                throw new ArgumentException("The month is not correct"); 
            }

            long[] interval = DateConvert.GetInterval(month);
            return await SearchEventsByIdProfile(idprofile, interval[0], interval[1]);
        }
    }
}
