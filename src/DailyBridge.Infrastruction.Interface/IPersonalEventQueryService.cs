﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data;

namespace DailyBridge.Infrastruction.Interface
{
    public interface IPersonalEventQueryService
    {
        Task<PersonalEvent> GetEventAsync(int id);
        /// <summary>
        /// Поиск события в поисковом движке по идентификатору.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>JSON LOL!!</returns>
        Task<string> SearchEventAsync(int id);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idprofile"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns>JSON ARRAY!</returns>
        Task<IEnumerable<string>> SearchEventsByIdProfile(int idprofile,long start, long end);
        Task<IEnumerable<string>> SearchEventsByIdProfileToEnd(int idprofile, long start);
        Task<IEnumerable<string>> SearchEventsByIdProfileStartNull(int idprofile, long end);
        Task<IEnumerable<string>> SearchEventsByIdProfile(int idprofile, int month);
    }
}
