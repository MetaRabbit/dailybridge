﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Data;

namespace DailyBridge.Infrastruction.Interface
{
    public interface IGeneralProfileQueryService
    {
        Task<GeneralProfile> GetProfileAsync( int id, bool isLite );
        Task<IEnumerable<GeneralProfile>> GetProfilesAsync(int[] ids);
        Task<IEnumerable<GeneralProfile>> SearchProfilesAsync( string text, int[] selectedUserIds, int numPage, int countProfileOnPage );
        Task<IEnumerable<GeneralProfile>> SearchProfilesAsync(int[] selectedUserIds, int numPage, int countProfileOnPage);


    }
}
