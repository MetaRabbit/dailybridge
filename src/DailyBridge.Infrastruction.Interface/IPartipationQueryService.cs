﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data;

namespace DailyBridge.Infrastruction.Interface
{
    public interface IPartipationQueryService
    {
        Task<IEnumerable<Participation>> GetParticipationsByIdEvent(int eventId);
        Task<Participation> GetParticipation(int eventId, int profId);
    }
}
