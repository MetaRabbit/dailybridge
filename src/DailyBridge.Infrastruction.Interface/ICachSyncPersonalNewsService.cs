﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data;

namespace DailyBridge.Infrastruction.Interface
{
    public interface ICachSyncPersonalNewsService
    {
        Task SyncData(News news, GeneralProfile generalProfile);

        Task<bool> SyncDb(int newsId);
    }
}
