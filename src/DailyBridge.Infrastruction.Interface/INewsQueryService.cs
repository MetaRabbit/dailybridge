﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data;

namespace DailyBridge.Infrastruction.Interface
{
    public interface INewsQueryService
    {       
        Task<News> GetNewsById(int newsId);
        Task<IEnumerable<News>> SearchNews(int eventId, int numPage, int countProfileOnPage, int? acceptNews);
    }
}
