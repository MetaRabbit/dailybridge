﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data;

namespace DailyBridge.Infrastruction.Interface
{
    public interface IContactQueryService
    {
        /// <summary>
        /// Страничная выборка контактов из базы данных. status : 0 - ActiveOnFrontEnd, 1 - Active, 2 - Blocked 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status">Статус контакта. 0 - ActiveOnFrontEnd, 1 - Active, 2 - Blocked</param>
        /// <param name="pageNumber">Номер страницы.</param>
        /// <param name="contactOnPage">Кол-во контактов на странице</param>
        /// <returns></returns>
        Task<IEnumerable<Contact>> GetContactsListAsync(int id, int status, int pageNumber, int contactOnPage);
        /// <summary>
        /// Выборка контактов из базы данных. status : 0 - ActiveOnFrontEnd, 1 - Active, 2 - Blocked 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status">Статус контакта. 0 - ActiveOnFrontEnd, 1 - Active, 2 - Blocked</param>
        /// <returns></returns>
        Task<IEnumerable<Contact>> GetContactsListAsync(int id, int status);
        Task<IEnumerable<Contact>> SearchContactsListAsync(int id, int pageNumber, int contactOnPage);
        Task<IEnumerable<Contact>> SearchContactsListAsync(int id);

    }
}
