﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DailyBridge.Infrastruction.Interface
{
    public interface ICachSynchLocationService
    {
        Task<bool> SyncLocationsByProfileId(string profileId);

        Task<bool> SyncLocationById(int locId);
    }
}
