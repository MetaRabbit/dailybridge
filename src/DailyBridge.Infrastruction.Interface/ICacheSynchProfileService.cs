﻿using System.Threading.Tasks;
using Data;

namespace DailyBridge.Infrastruction.Interface
{
    public interface ICacheSynchProfileService
    {
        void SyncData(GeneralProfile profile);

        Task<bool> TrySyncDbAsync(int id, bool isLite);
    }
}
