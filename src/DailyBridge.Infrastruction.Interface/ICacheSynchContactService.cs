﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data;

namespace DailyBridge.Infrastruction.Interface
{
    public interface ICacheSynchContactService
    {
        Task<bool> SyncFrondEndContacts(int profileId);
        Task<bool> SyncContact(string idSource, string idTarget, int status);
    }
}
