﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data;

namespace DailyBridge.Infrastruction.Interface
{
    public interface ICachSynchPersonalEventService
    {
        Task<bool> SyncById(int id);
        Task<bool> SyncData(PersonalEvent personalEvent, PersonalLocation location, IEnumerable<Participation> participations);
        Task<bool> SyncOnMonthByProfileId(int id);

    }
}
