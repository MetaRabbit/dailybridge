﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data;

namespace DailyBridge.Infrastruction.Interface
{
    public interface IPersonalLocationQueryService
    {
        Task<IEnumerable<PersonalLocation>> SearchPersonalLocationsAsync(string text, string idProfile);
        Task<PersonalLocation> SearchPersonalLocationAsync(string idLocation);

        Task<IEnumerable<PersonalLocation>> GetPersonalLocationsAsync(string idProfile);
        Task<PersonalLocation> GetPersonalLocationAsync(int idLocation);

    }
}
