﻿using System;
using System.Threading.Tasks;
using DailyBridge.Domain.Interface.Entity;
using DailyBridge.Domain.Interface.Identity;

namespace DailyBridge.Domain.Interface.Service
{
    public interface IGeneralProfileRepository
    {
        Task<bool> AddAsync(IGeneralProfile profile);
        Task<IGeneralProfile> GetProfileAsync(IGeneralProfileId idUser);
        Task<bool> ExistsAsync(IGeneralProfileId id);
        bool Exists(IGeneralProfileId id);
        Task UpdateAsync(IGeneralProfile profile);
        //THIS IS CONTACT TIME!
        Task<IContact> GetContactAsync(IContactId contact);
        /// <summary>
        /// Попытка добавления контакта. Вернет FALSE, если 
        /// -не существует одного из профилей; 
        /// -контакт уже существует;
        /// -или равен null. 
        /// </summary>
        /// <param name="contact"></param>
        /// <returns></returns>
        Task<bool> TryAddContactAsync(IContact contact);
        Task<bool> DeleteAsync(IContactId contact);
        Task<bool> ExistsAsync(IContactId contactId);
        bool Exists(IContactId contactId);
        Task UpdateAsync(IContact profile);
        //THIS IS EVENT TIME!
        Task<IPersonalEvent> GetEventAsync(IPersonalEventId personalEventId);
        Task<int> AddAsync(IPersonalEvent personalEvent, IParticipation participation);
        Task<bool> DeleteAsync(IPersonalEventId personalEventId);
        Task<bool> ExistsAsync(IGeneralProfileId id, IPersonalEvent personalEvent);
        Task<bool> UpdateAsync(IPersonalEvent profile, bool p);
        Task<bool> AddNotice(IPersonalEventId personalEventId, long timeoffset);
        //THIS IS LOCATION TIME! WTF?
        Task<IPersonalLocation> GetLocationsAsync(IGeneralProfile id);
        Task<IPersonalLocation> GetLocationAsync(ILocationId id);
        Task<bool> AddAsync(IPersonalLocation personalLocation);
        Task UpdateAsync(IPersonalLocation personalLocation);
        Task<bool> DeleteAsync(IGeneralProfileId profId, ILocationId id);
        //THIS IS PARTICIPATION TIME! 
        //Task<bool> AddNewEventAsync(IPersonalEvent personalEvent, IParticipation participation);
        //THIS IS NEWS TIME
        Task<bool> AddAsync(IGeneralProfileId profileId, INews news);
        Task<bool> UpdateAuthorPrivilegesAsync(IGeneralProfileId profileId, INews news);
        Task<bool> UpdateCreaterPrivilegesAsync(IGeneralProfileId profileId, INews news);
    }
}
