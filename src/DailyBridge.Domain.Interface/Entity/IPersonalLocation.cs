﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DailyBridge.Domain.Interface.Identity;
using DailyBridge.Domain.Interface.ValueObject;

namespace DailyBridge.Domain.Interface.Entity
{
    public interface IPersonalLocation
    {
        ILocationId LocationId { get; }
        IGeneralProfileId GeneralProfileId { get; }
        ILocationInformation LocationInformation { get; }
        string Address { get; }

        IPersonalLocation ChangeLocationInformation(ILocationInformation locationInformation);
        IPersonalLocation ChangeAddress(string address);
    }
}
