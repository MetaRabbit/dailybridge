﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DailyBridge.Domain.Interface.Enumeration;
using DailyBridge.Domain.Interface.Identity;
using DailyBridge.Domain.Interface.ValueObject;

namespace DailyBridge.Domain.Interface.Entity
{
    public interface IPersonalEvent
    {
        IPersonalEventId PersonalEventId { get; }
        ILocationId Location { get; }
        long TimeOffset { get; }
        bool IsPeriodicity { get; }
        IPersonalEventInformation EventInformation { get; }
        IReadOnlyList<IGeneralProfile> RelatedProfiles { get; }
        TypeEvent TypeEvent { get; }

        IPersonalEvent ChangeTimeOffset(long time);
        IPersonalEvent SwitchPeriodicity(bool isPeriodicity);
        IPersonalEvent ChangeEventInformation(IPersonalEventInformation eventInformation);
        IPersonalEvent AddRelatedProfile(IGeneralProfile generalProfile);
        IPersonalEvent RemoveRelatedProfile(IGeneralProfile generalProfile);
        IPersonalEvent ChangeType(TypeEvent typeEvent);
    }
}
