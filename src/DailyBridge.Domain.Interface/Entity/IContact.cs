﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DailyBridge.Domain.Interface.Enumeration;
using DailyBridge.Domain.Interface.Identity;

namespace DailyBridge.Domain.Interface.Entity
{
    public interface IContact
    {
        IContactId ContactId { get; }
        TypeContact Status { get; }
        IContact Switch(TypeContact typeContact);
    }
}
