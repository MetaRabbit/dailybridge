﻿using DailyBridge.Domain.Interface.Enumeration;
using DailyBridge.Domain.Interface.Identity;

namespace DailyBridge.Domain.Interface.Entity
{
    public interface IParticipation
    {

        IParticipationId ParticipationId { get; }
        string Comment { get; }
        TypeParticipation Type { get; }

        IParticipation ChangeComment(string comment);
        IParticipation ChangeType(TypeParticipation typeParticipation);

    }
}
