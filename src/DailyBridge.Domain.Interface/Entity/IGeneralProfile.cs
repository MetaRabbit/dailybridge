﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DailyBridge.Domain.Interface.Enumeration;
using DailyBridge.Domain.Interface.Identity;
using DailyBridge.Domain.Interface.ValueObject;

namespace DailyBridge.Domain.Interface.Entity
{
    public interface IGeneralProfile
    {
        IGeneralProfile ChangeEmail(IEmail email);
        IGeneralProfile ChangeAvatar(string url);
        IGeneralProfile ChangeStateProfile(StatusGeneralProfile state);
        IGeneralProfile ChangeFullName(IFullName fullName);
        IGeneralProfile ChangePhoneNumber(IPhoneNumber phoneNumber);
        IGeneralProfileId ProfileId { get; }
        IFullName FullName { get; }
        IEmail Email { get; }
        StatusGeneralProfile StatusProfile { get; }
        IAvatar Avatar { get; set; }
        IPhoneNumber PhoneNumber { get; }
    }
}
