﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DailyBridge.Domain.Interface.Enumeration;
using DailyBridge.Domain.Interface.Identity;
using DailyBridge.Domain.Interface.ValueObject;

namespace DailyBridge.Domain.Interface.Entity
{
    public interface INews
    {
         INewsId NewsId { get; }
         IPersonalEventId PersonalEventId { get; }
         TypeNews Type { get; }
         TypeNotify TypeNotify { get; }
         IGeneralProfileId AuthorId { get; }
         long TimeOffset { get; }
         INewsInformation Information { get; }
         StatusNews Status { get; }

         INews ChangeInformation(INewsInformation information);
         INews ChangeTypeNotify(TypeNotify typeNotify);
         INews ChangeStatus(StatusNews status);
    }
}
