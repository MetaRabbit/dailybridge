﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DailyBridge.Domain.Interface.Identity
{
    public interface IContactId : IIdentity
    {
        IGeneralProfileId SourceId { get; }
        IGeneralProfileId TargetId { get; }
    }
}
