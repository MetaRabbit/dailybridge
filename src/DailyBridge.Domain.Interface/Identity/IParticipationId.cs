﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DailyBridge.Domain.Interface.Identity
{
    public interface IParticipationId : IIdentity
    {
        IGeneralProfileId ParticipantId { get; }
        IPersonalEventId EventId { get; }
    }
}
