﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DailyBridge.Domain.Interface.ValueObject
{
    public interface IPersonalEventInformation : IValueObject
    {
        string Name { get; }
        string Description { get; }
        string AvatarUrl { get; }
    }
}
