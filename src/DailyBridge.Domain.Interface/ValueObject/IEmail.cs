﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DailyBridge.Domain.Interface.ValueObject
{
    public interface IEmail
    {
        string Name { get;  }
        string Domain { get;}

        string Full { get; }
    }
}
