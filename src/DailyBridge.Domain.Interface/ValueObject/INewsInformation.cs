﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DailyBridge.Domain.Interface.ValueObject
{
    public interface INewsInformation
    {
        string Heading { get; }
        string SubHeading { get; }
        string Text { get; }

    }
}
