﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DailyBridge.Domain.Interface.Enumeration
{
    public static class EnumHelper
    {
        public static bool IsOnlyFrondEndContactList(string[] types)
        {
            TypeContact typeContact;
            return types.Length == 1 && Enum.TryParse(types[0], true, out typeContact) &&
                   typeContact == TypeContact.ActiveOnFrontEnd;
        }
    }

    public enum StatusGeneralProfile
    {
        Active,
        Blocked
    }

    public enum TypeContact
    {
        ActiveOnFrontEnd,
        Active,
        Blocked
    }

    public enum TypeParticipation
    {
        Active,
        Blocked,
        Invited,
        Creater
    }

    public enum TypeEvent
    {
        Public,
        Private
    }

    public enum TypeNews
    {
        Info,
        Notice
    }

    public enum TypeNotify
    {
        Info,
        Warning,
        Error,
        Critical,
        Debug
    }

    public enum StatusNews
    {
        NotChecked,
        Checked
    }

}
