﻿using System;
using DailyBridge.Domain.Identity;
using DailyBridge.Domain.Interface.Entity;
using DailyBridge.Domain.Interface.Enumeration;
using DailyBridge.Domain.ValueObject;
using Data;

namespace DailyBridge.Domain.Service
{

    public static class GeneralProfileConvertService
    {
        #region Convert to DTO
        public static GeneralProfile ConvertToDto(IGeneralProfile profile)
        {
            return new GeneralProfile()
            {
                Email = profile.Email.Full,
                FirstName = profile.FullName.FirstName,
                LastName = profile.FullName.LastName,
                MiddleName = profile.FullName.MiddleName,
                Status = (int)profile.StatusProfile,
                PhoneNumber = profile.PhoneNumber.Value,
                AvatarUrl = "NoAvatar",
                GeneralProfileId = Convert.ToInt32(profile.ProfileId.Id)
            };
        }

        public static Contact ConvertToDto(IContact contact)
        {
            return new Contact
            {
                Target = Convert.ToInt32(contact.ContactId.TargetId.Id),
                Source = Convert.ToInt32(contact.ContactId.SourceId.Id),
                Status = (int)contact.Status
            };
        }

        public static PersonalLocation ConvertToDto(IPersonalLocation personalLocation)
        {
            return new PersonalLocation
            {
                PersonalLocationId = Convert.ToInt32(personalLocation.LocationId.Id),
                Name = personalLocation.LocationInformation.Name,
                Description = personalLocation.LocationInformation.Description,
                GeneralProfileId = Convert.ToInt32(personalLocation.GeneralProfileId.Id),
                Address = string.IsNullOrEmpty(personalLocation.Address) ? "" : personalLocation.Address,
                PersonalEvent = null
            };
        }
        public static PersonalEvent ConvertToDto(IPersonalEvent personalEvent)
        {
            return new PersonalEvent
            {
                PersonalLocationId = Convert.ToInt32(personalEvent.Location.Id),
                Name = personalEvent.EventInformation.Name,
                Description = personalEvent.EventInformation.Description,
                TimeOffset = personalEvent.TimeOffset,
                PersonalEventId = Convert.ToInt32(personalEvent.PersonalEventId.Id),
                IsPerodicity = personalEvent.IsPeriodicity,
                AvatarUrl = personalEvent.EventInformation.AvatarUrl,
                Type = (int)personalEvent.TypeEvent
                };
        }
        public static Participation ConvertToDto(IParticipation participation)
        {
            return new Participation
            {
                PersonalEventId = Convert.ToInt32(participation.ParticipationId.EventId.Id),
                GeneralProfileId = Convert.ToInt32(participation.ParticipationId.ParticipantId.Id),
                Type  = (int)participation.Type,
                Comment = participation.Comment ?? null

            };
        }

        public static News ConvertToDto(INews news)
        {
            return new News
            {
                PersonalEventId = Convert.ToInt32(news.PersonalEventId.Id),
                NewsId = Convert.ToInt32(news.NewsId.Id),
                Type = (int)news.Type,
                AuthorId = Convert.ToInt32(news.AuthorId.Id),
                Heading = news.Information.Heading,
                SubHeading = news.Information.SubHeading ?? null,
                Text = news.Information.Text,
                TimeOffset = news.TimeOffset,
                Status = (int)news.Status
            };
        }
        #endregion
        public static IGeneralProfile ConvertToDomainOject(GeneralProfile profile)
        {
            var domainProfile = Entity.GeneralProfile
                .CreateProfile(GeneralProfileId.Create(profile.GeneralProfileId.ToString()),
                    Email.CreateEmailFromString(profile.Email),
                    FullName.CreateFullName(profile.FirstName, profile.LastName, profile.MiddleName),
                    PhoneNumber.CreatePhoneNumber(profile.PhoneNumber));

            return domainProfile;
        }
        public static IPersonalEvent ConvertToDomainOject(PersonalEvent personalEvent)
        {
            var domainProfile = Entity.PersonalEvent
                .CreatePersonalEvent(PersonalEventId.Create(personalEvent.PersonalEventId.ToString()), LocationId.Create(personalEvent.PersonalLocationId.ToString()),personalEvent.TimeOffset, personalEvent.IsPerodicity,
                    PersonalEventInformation.Create(personalEvent.Name,personalEvent.Description,personalEvent.AvatarUrl),  (TypeEvent)personalEvent.Type);

            return domainProfile;
        }

        public static IContact ConvertToDomainOject(Contact contact)
        {
            return Entity.Contact.CreateContact(ContactId.Create(GeneralProfileId.Create(contact.Source.ToString()), GeneralProfileId.Create(contact.Target.ToString())), (TypeContact)contact.Status);
        }

        public static IPersonalLocation ConvertToDomainOject(PersonalLocation location)
        {
            return Entity.PersonalLocation.Create(LocationId.Create(location.PersonalLocationId.ToString()), GeneralProfileId.Create(location.GeneralProfileId.ToString()),
                LocationInformation.CreateLocationInfo(location.Name, location.Description),
                location.Address);
        }

        public static INews ConvertToDomainOject(News news)
        {
            return Entity.News.CreateNews(NewsId.Create(news.NewsId.ToString()),
                PersonalEventId.Create(news.PersonalEventId.ToString()),
                (TypeNews)news.Type, (TypeNotify)news.TypeNotify,
                NewsInformation.Create(news.Heading, news.SubHeading, news.Text), GeneralProfileId.Create(news.AuthorId.ToString()), news.TimeOffset, (StatusNews)news.Status);
        }
    }
}
