﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DailyBridge.Domain.Interface.ValueObject;
using DailyBridge.Domain.Utils;

namespace DailyBridge.Domain.ValueObject
{
    public class LocationInformation : Common.ValueObject, ILocationInformation
    {

        public string Name { get; }
        public string Description { get; }

        private LocationInformation(string name, string description)
        {
            Name = name;
            Description = description;
        }

        public override IEnumerable<object> GetEqualityComponents()
        {
            yield return Name;
            yield return Description;
        }

        public static LocationInformation CreateLocationInfo(string name, string description)
        {
            if (string.IsNullOrEmpty(name))
            {
                return null;
            }
            return new LocationInformation(name, description);
        }
    }
}
