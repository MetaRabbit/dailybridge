﻿using System.Collections.Generic;
using DailyBridge.Domain.Interface.ValueObject;
using DailyBridge.Domain.Utils;

namespace DailyBridge.Domain.ValueObject
{
    public class PhoneNumber : Common.ValueObject, IPhoneNumber
    {
        private PhoneNumber(string codeZone,string value)
        {
            Value = value;
            CodeZone = codeZone;
        }
        public string Value { get; private set; }
        public string CodeZone { get; private set; }
        public override IEnumerable<object> GetEqualityComponents()
        {
            yield return Value;
            yield return CodeZone;
        }
        /// <summary>
        /// The code zone and value must be separated "_"
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static PhoneNumber CreatePhoneNumber(string number)
        {
            if (number==null || !number.Contains("_") || number.Length-1 == number.IndexOf('_'))
            {
                return null;
            }
            var codeZone = number.Substring(0, number.IndexOf('_'));
            var value = number.Substring(number.IndexOf('_') + 1);
            return Validator.VerifyPhoneNumber(codeZone, value) ? new PhoneNumber(codeZone, value) : null;
        }
    }
}
