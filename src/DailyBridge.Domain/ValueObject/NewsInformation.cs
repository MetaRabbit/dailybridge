﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DailyBridge.Domain.Interface.ValueObject;

namespace DailyBridge.Domain.ValueObject
{
    public class NewsInformation : INewsInformation
    {
        public NewsInformation(string heading, string subHeading, string text)
        {
            Heading = heading;
            SubHeading = subHeading;

            Text = text;
        }

        public string Heading { get; }
        public string SubHeading { get; }
        public string Text { get; }

        public static NewsInformation Create(string heading, string subHeading, string text)
        {
            if (string.IsNullOrEmpty(heading))
            {
                return null;
            }

            return new NewsInformation(heading, subHeading, text);
        }

    }
}
