﻿using System.Collections.Generic;
using DailyBridge.Domain.Interface.ValueObject;

namespace DailyBridge.Domain.ValueObject
{
    public class Avatar : Common.ValueObject, IAvatar
    {
        public Avatar(string url)
        {
            Url = url;
        }
        public string Url { get; private set; }
        public override IEnumerable<object> GetEqualityComponents()
        {
            yield return Url;
        }
    }
}
