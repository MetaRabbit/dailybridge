﻿using System.Collections.Generic;
using DailyBridge.Domain.Interface.ValueObject;
using DailyBridge.Domain.Utils;

namespace DailyBridge.Domain.ValueObject
{
    public class FullName : Common.ValueObject, IFullName
    {
        private FullName(string firstName, string lastName, string middleName)
        {
            FirstName = firstName;
            LastName = lastName;
            MiddleName = middleName;
        }
        public string FirstName { get;private set; }
        public string LastName { get; private set; }
        public string MiddleName { get; private set; }
        public string Full => FirstName + " " + LastName;
        public override IEnumerable<object> GetEqualityComponents()
        {
            yield return FirstName;
            yield return LastName;
            yield return MiddleName;
        }

        public static FullName CreateFullName(string firstName, string lastName, string middleName)
        {
            if (!Validator.VerifyProfileName(firstName) || !Validator.VerifyProfileName(lastName))
            {
                return null;
            }
            if (string.IsNullOrEmpty(middleName) || !Validator.VerifyProfileName(middleName))
            {
                return new FullName(firstName, lastName, "");
            }
            return Validator.VerifyProfileName(middleName) ? new FullName(firstName, lastName, middleName) : null;
        }
    }
}
