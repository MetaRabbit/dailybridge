﻿using System.Collections.Generic;
using DailyBridge.Domain.Interface.ValueObject;
using DailyBridge.Domain.Utils;

namespace DailyBridge.Domain.ValueObject
{
    public class Email : Common.ValueObject, IEmail
    {
        private Email(string name, string domain)
        {
            Name = name;
            Domain = domain;
        }

        public string Name { get; private set; }
        public string Domain { get; private set; }
        public string Full => Name + "@" + Domain;

        public override IEnumerable<object> GetEqualityComponents()
        {
            yield return Name;
            yield return Domain;
        }

        public static Email CreateEmailFromString(string email)
        {
            if (email ==null || !email.Contains("@") || email.Length-1 == email.IndexOf('@'))
            {
                return null;
            }
            var name = email.Substring(0, email.IndexOf('@'));
            var domain = email.Substring(email.IndexOf('@')+1);
            return Validator.VerifyEmail(name, domain) ? new Email(name, domain) : null;
        }
    }
}
