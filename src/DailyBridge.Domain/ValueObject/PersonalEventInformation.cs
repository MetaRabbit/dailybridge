﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DailyBridge.Domain.Interface.ValueObject;
using DailyBridge.Domain.Utils;

namespace DailyBridge.Domain.ValueObject
{
    public class PersonalEventInformation : Common.ValueObject, IPersonalEventInformation
    {
        public PersonalEventInformation(string name, string description, string avatarUrl)
        {
            Name = name;
            Description = description;
            AvatarUrl = avatarUrl;
        }

        public override IEnumerable<object> GetEqualityComponents()
        {
            yield return Name;
            yield return Description;
        }

        public string Name { get; }
        public string Description { get; }
        public string AvatarUrl { get; }

        public static PersonalEventInformation Create(string name, string description, string avatarUrl)
        {
            if (string.IsNullOrEmpty(name))
            {
                return null;
            }
            return new PersonalEventInformation(name, description, avatarUrl);
        }
    }
}
