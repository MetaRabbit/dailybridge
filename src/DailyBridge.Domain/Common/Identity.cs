﻿using DailyBridge.Domain.Interface.Identity;

namespace DailyBridge.Domain.Common
{
    public class Identity : IIdentity
    {
        protected Identity()
        {
            Id = Utils.IdGenetator.GetId();
        }

        protected Identity(string id)
        {
            Id = id;
        }
        public string Id { get; private set; }

        public override bool Equals(object obj)
        {
            if (object.ReferenceEquals(this, obj)) return true;
            if (object.ReferenceEquals(null, obj)) return false;
            return this.Id.Equals(((Identity)obj).Id);
        }

        public bool Equals(Identity id)
        {
            if (object.ReferenceEquals(this, id)) return true;
            if (object.ReferenceEquals(null, id)) return false;
            return this.Id.Equals(id.Id);
        }
    }
}
