﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DailyBridge.Domain.Common
{
    public abstract class EntityWithCompositeId
    {
        protected abstract IEnumerable<object> GetComponentsIdentity();

        public override bool Equals(object obj)
        {
            if (object.ReferenceEquals(this, obj)) return true;
            if (object.ReferenceEquals(null, obj)) return false;
            if (GetType() != obj.GetType()) return false;
            var other = obj as EntityWithCompositeId;
            return GetComponentsIdentity().SequenceEqual(other.GetComponentsIdentity());
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

}
