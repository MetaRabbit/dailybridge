﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DailyBridge.Domain.Interface.Entity;
using DailyBridge.Domain.Interface.Identity;
using DailyBridge.Domain.Interface.ValueObject;

namespace DailyBridge.Domain.Entity
{
    public class PersonalLocation : IPersonalLocation
    {

        private PersonalLocation(ILocationId locationId, IGeneralProfileId generalProfileId)
        {
            LocationId = locationId;
            GeneralProfileId = generalProfileId;
        }

        public ILocationId LocationId { get; }
        public IGeneralProfileId GeneralProfileId { get; }
        public IPersonalEventId PersonalEventId { get; private set; }
        public ILocationInformation LocationInformation { get; private set; }
        public string Address { get; private set; }

        public IPersonalLocation ChangeLocationInformation(ILocationInformation locationInformation)
        {
            if (locationInformation != null)
            {
                LocationInformation = locationInformation;
            }          
            return this;
        }

        public IPersonalLocation ChangeAddress(string address)
        {
            if (!string.IsNullOrEmpty(address))
            {
                Address = address;
            }
           
            return this;
        }

        public static PersonalLocation Create(ILocationId locationId, IGeneralProfileId generalProfileId, ILocationInformation locationInformation, string address)
        {
            if (locationId == null || generalProfileId == null || locationInformation == null)
            {
                return null;
            }

            var loc = new PersonalLocation(locationId, generalProfileId);
            loc.ChangeLocationInformation(locationInformation);
            loc.ChangeAddress(address);
            return loc;

        }
    }
}
