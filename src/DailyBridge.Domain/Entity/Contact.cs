﻿using DailyBridge.Domain.Identity;
using DailyBridge.Domain.Interface.Entity;
using DailyBridge.Domain.Interface.Enumeration;
using DailyBridge.Domain.Interface.Identity;
using DailyBridge.Domain.Utils;

namespace DailyBridge.Domain.Entity
{
    //Id сущности формируется из Id Source и Target
    public class Contact : IContact
    {
        private Contact(ContactId contactId)
        {
            ContactId = contactId;
        }
        public IContactId ContactId { get; private set; }
        public TypeContact Status { get; private set; }
        public IContact Switch(TypeContact typeContact)
        {
            Status = typeContact;
            return this;
        }

        public static Contact CreateContact(ContactId contactId, TypeContact type)
        {
            if (contactId==null )
            {
                return null;
            }

            var frShip = new Contact(contactId) {Status = type};
            //0 - доступ к информации профиля
            return frShip;
        }

    }
}
