﻿using DailyBridge.Domain.Interface.Entity;
using DailyBridge.Domain.Interface.Enumeration;
using DailyBridge.Domain.Interface.Identity;
using DailyBridge.Domain.Interface.ValueObject;

namespace DailyBridge.Domain.Entity
{
    public class News : INews
    {
        private News(INewsId newsId, IPersonalEventId personalEventId, TypeNews type, TypeNotify typeNotify, IGeneralProfileId authorId, long timeOffset)
        {
            NewsId = newsId;
            PersonalEventId = personalEventId;
            Type = type;
            TypeNotify = typeNotify;
            AuthorId = authorId;
            TimeOffset = timeOffset;
        }

        public INewsId NewsId { get; }
        public IPersonalEventId PersonalEventId { get; }
        public TypeNews Type { get; }
        public TypeNotify TypeNotify { get; private set; }
        public IGeneralProfileId AuthorId { get; }
        public long TimeOffset { get; }
        public INewsInformation Information { get; private set; }
        public StatusNews Status { get; private set; }

        public INews ChangeInformation(INewsInformation information)
        {
            if (information==null)
            {
                return this;
            }
            Information = information;
            return this;
        }

        public INews ChangeTypeNotify(TypeNotify typeNotify)
        {
            TypeNotify = typeNotify;
            return this;
        }

        public INews ChangeStatus(StatusNews status)
        {
            Status = status;
            return this;
        }

        public static INews CreateNews(INewsId newsId, IPersonalEventId personalEventId, TypeNews type, TypeNotify typeNotify, INewsInformation information, IGeneralProfileId authorId, long timeOffset, StatusNews status)
        {
            //TODO проверка кооректности timeOffset
            if (newsId == null || personalEventId == null || information == null || timeOffset <= 0 || authorId == null)
            {
                return null;
            }
            var news = new News(newsId, personalEventId, type, typeNotify, authorId, timeOffset).ChangeInformation(information).ChangeStatus(status);
            return news;
        }

        public static INews CreateNotCheckedNewsFakeId(IPersonalEventId personalEventId, TypeNews type, TypeNotify typeNotify, INewsInformation information, IGeneralProfileId authorId, long timeOffset)
        {
            //TODO проверка кооректности timeOffset
            if (personalEventId == null || information == null || timeOffset <= 0 || authorId == null)
            {
                return null;
            }
            var news = new News(Identity.NewsId.Create("0"), personalEventId, type, typeNotify, authorId, timeOffset).ChangeInformation(information).ChangeStatus(StatusNews.NotChecked);
            return news;

        }
    }
}
