﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DailyBridge.Domain.Interface.Entity;
using DailyBridge.Domain.Interface.Enumeration;
using DailyBridge.Domain.Interface.Identity;
using DailyBridge.Domain.Interface.ValueObject;

namespace DailyBridge.Domain.Entity
{
    public class PersonalEvent : IPersonalEvent
    {
        public PersonalEvent(IPersonalEventId personalEventId, bool isPeriodicity, long timeOffset, ILocationId location, IPersonalEventInformation eventInformation, TypeEvent typeEvent)
        {
            PersonalEventId = personalEventId;
            IsPeriodicity = isPeriodicity;
            TimeOffset = timeOffset;
            Location = location;
            EventInformation = eventInformation;
            TypeEvent = typeEvent;
        }

        public IPersonalEventId PersonalEventId { get; }
        public ILocationId Location { get; private set; }
        public long TimeOffset { get; private set; }
        public bool IsPeriodicity { get; private set; }
        public IPersonalEventInformation EventInformation { get; private set; }
        public IReadOnlyList<IGeneralProfile> RelatedProfiles { get; }
        public TypeEvent TypeEvent { get; private set; }

        public IPersonalEvent ChangeTimeOffset(long time)
        {
            if (time > 300 )
            {
                TimeOffset = time;
            }          
            return this;
        }

        public IPersonalEvent SwitchPeriodicity(bool isPeriodicity)
        {
            IsPeriodicity = isPeriodicity;
            return this;
        }

        public IPersonalEvent ChangeEventInformation(IPersonalEventInformation eventInformation)
        {
            if (eventInformation!=null)
            {
                EventInformation = eventInformation;
            }
            return this;
        }

        public IPersonalEvent AddRelatedProfile(IGeneralProfile generalProfile)
        {
            throw new NotImplementedException();
        }

        public IPersonalEvent RemoveRelatedProfile(IGeneralProfile generalProfile)
        {
            throw new NotImplementedException();
        }

        public IPersonalEvent ChangeType(TypeEvent typeEvent)
        {
            TypeEvent = typeEvent;
            return this;
        }

        public static PersonalEvent CreatePrivatePersonalEvent(IPersonalEventId personalEventId, ILocationId location, long timeOffset, bool isPeriodicity, IPersonalEventInformation eventInformation)
        {
            if (personalEventId == null || location == null || eventInformation == null)
            {
                return null;
            }
            return new PersonalEvent(personalEventId, isPeriodicity, timeOffset, location, eventInformation, TypeEvent.Private);

        }
        public static PersonalEvent CreatePersonalEvent(IPersonalEventId personalEventId, ILocationId location, long timeOffset, bool isPeriodicity, IPersonalEventInformation eventInformation, TypeEvent typeEvent )
        {
            if (personalEventId == null || location == null || eventInformation == null)
            {
                return null;
            }
            return new PersonalEvent(personalEventId, isPeriodicity, timeOffset, location, eventInformation, typeEvent);

        }
    }
}
