﻿using DailyBridge.Domain.Interface.Entity;
using DailyBridge.Domain.Interface.Enumeration;
using DailyBridge.Domain.Interface.Identity;
using DailyBridge.Domain.Interface.ValueObject;
using DailyBridge.Domain.Utils;
using DailyBridge.Domain.ValueObject;

namespace DailyBridge.Domain.Entity
{
    public class GeneralProfile : IGeneralProfile
    {
        private GeneralProfile(IGeneralProfileId id,IEmail email, IFullName fullName, IPhoneNumber phoneNumber)
        {
            ProfileId = id;
            Email = email;
            FullName = fullName;
            PhoneNumber = phoneNumber;
        }
        public IGeneralProfileId ProfileId { get; private set; }
        public IFullName FullName { get; private set; }
        public IEmail Email { get;private set; }
        public StatusGeneralProfile StatusProfile { get; private set; }
        public IAvatar Avatar { get; set; }
        public IPhoneNumber PhoneNumber { get; private set; }
        public IGeneralProfile ChangeEmail(IEmail email)
        {
            if (email!=null)
            {
                Email = email;
            }
            return this;
        }

        public IGeneralProfile ChangeAvatar(string url)
        {
            Avatar = new Avatar(url);
            return this;
        }
        public IGeneralProfile ChangeStateProfile(StatusGeneralProfile state)
        {
            StatusProfile = state;
            return this;
        }
        public IGeneralProfile ChangeFullName(IFullName fullName)
        {
            if (fullName!=null)
            {
                FullName = fullName;
            }
            return this;
        }
        public IGeneralProfile ChangePhoneNumber(IPhoneNumber phoneNumber)
        {
            if (phoneNumber != null)
            {
                PhoneNumber = phoneNumber;
            }          
            return this;
        }
        public static IGeneralProfile CreateProfile(IGeneralProfileId id, IEmail email, IFullName fullName, IPhoneNumber phoneNumber)
        {
            if (email == null || id == null || fullName == null) return null;
            return new GeneralProfile(id, email, fullName, phoneNumber);
        }
    }

}
