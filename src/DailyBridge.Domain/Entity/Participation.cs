﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DailyBridge.Domain.Interface.Entity;
using DailyBridge.Domain.Interface.Enumeration;
using DailyBridge.Domain.Interface.Identity;

namespace DailyBridge.Domain.Entity
{
    public class Participation : IParticipation
    {
        private Participation(IParticipationId participationId, TypeParticipation type)
        {
            ParticipationId = participationId;
            Type = type;
        }


        public IParticipationId ParticipationId { get;}
        public string Comment { get; private set; }
        public TypeParticipation Type { get; private set; }
        public IParticipation ChangeComment(string comment)
        {
            if (string.IsNullOrEmpty(comment))
            {
                return this;
            }
            Comment = comment;
            return this;
        }

        public IParticipation ChangeType(TypeParticipation typeParticipation)
        {
            Type = typeParticipation;
            return this;
        }

        public static Participation CreateCreaterUnit(IParticipationId participationId)
        {
            if (participationId==null)
            {
                return null;
            }
            var participation = new Participation(participationId, TypeParticipation.Creater);
            return participation;
        }

        public static Participation CreateInvitedUnit(IParticipationId participationId, string comment)
        {
            if (participationId == null)
            {
                return null;
            }
            var participation = new Participation(participationId, TypeParticipation.Active);
            participation.ChangeComment(comment);
            return participation;
        }
    }
}
