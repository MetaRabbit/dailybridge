﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DailyBridge.Domain.Interface.Identity;

namespace DailyBridge.Domain.Identity
{
    public class ParticipationId : Common.Identity, IParticipationId
    {
        private ParticipationId(IPersonalEventId eventId, IGeneralProfileId participantId)
            : base(eventId.Id + ":" + participantId.Id)
        {
        }

        public IGeneralProfileId ParticipantId => GeneralProfileId.Create(this.Id.Split(':')[1]);
        public IPersonalEventId EventId => PersonalEventId.Create(this.Id.Split(':')[0]);

        public static ParticipationId Create(IPersonalEventId eventId, IGeneralProfileId participantId)
        {
            if (eventId == null || participantId == null)
            {
                return null;
            }

            return new ParticipationId(eventId, participantId);
        }
    }
}
