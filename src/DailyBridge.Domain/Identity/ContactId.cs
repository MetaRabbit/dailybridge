﻿using DailyBridge.Domain.Interface.Entity;
using DailyBridge.Domain.Interface.Identity;
using DailyBridge.Domain.Utils;

namespace DailyBridge.Domain.Identity
{
    public class ContactId : Common.Identity, IContactId
    {
        private ContactId(IGeneralProfileId sourceId, IGeneralProfileId targetId)
            : base(sourceId.Id + ":" + targetId.Id)
        {
        }

        public IGeneralProfileId SourceId =>  GeneralProfileId.Create(this.Id.Split(':')[0]);
        public IGeneralProfileId TargetId =>  GeneralProfileId.Create(this.Id.Split(':')[1]);


        public static ContactId Create(IGeneralProfileId sourceId, IGeneralProfileId targetId)
        {
            if (sourceId==null || targetId==null)
            {
                return null;
            }
           
            return new ContactId(sourceId, targetId);
        }
    }
}
