﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DailyBridge.Domain.Interface.Identity;
using DailyBridge.Domain.Utils;

namespace DailyBridge.Domain.Identity
{
    public class PersonalEventId : IPersonalEventId
    {
        public PersonalEventId(string id)
        {
            Id = id;
        }

        public string Id { get; }

        public static PersonalEventId Create(string id)
        {
            return Validator.VerifyProfileId(id) ? new PersonalEventId(id) : null;
        }
    }
}
