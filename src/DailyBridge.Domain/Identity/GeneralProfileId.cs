﻿using DailyBridge.Domain.Interface.Identity;
using DailyBridge.Domain.Utils;

namespace DailyBridge.Domain.Identity
{
    public class GeneralProfileId : Common.Identity, IGeneralProfileId
    {
        private GeneralProfileId()
            : base()
        {
        }

        private GeneralProfileId(string id)
            : base(id)
        {
        }

        public static GeneralProfileId Create(string id)
        {
            return !Validator.VerifyProfileId(id) ? null : new GeneralProfileId(id);
        }
    }
}
