﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DailyBridge.Domain.Interface.Identity;
using DailyBridge.Domain.Utils;

namespace DailyBridge.Domain.Identity
{
    public class LocationId : Common.Identity,ILocationId
    {
        private LocationId(string id) :base(id)
        {

        }


        public static LocationId Create(string id)
        {
            return !Validator.VerifyProfileId(id) ? null : new LocationId(id);
        }
    }
}
