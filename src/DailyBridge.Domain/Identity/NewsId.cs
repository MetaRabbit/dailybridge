﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DailyBridge.Domain.Interface.Identity;
using DailyBridge.Domain.Utils;

namespace DailyBridge.Domain.Identity
{
    public class NewsId : Common.Identity, INewsId
    {
        private NewsId(string id) :base(id)
        {
        }

        public static NewsId Create(string id)
        {
            return !Validator.VerifyProfileId(id) ? null : new NewsId(id);
        }
    }
}
