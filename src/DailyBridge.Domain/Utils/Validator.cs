﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DailyBridge.Domain.Interface.Identity;
using Remotion.Linq.Parsing.Structure.IntermediateModel;

namespace DailyBridge.Domain.Utils
{
    public class Validator
    {
        private static readonly string[] Atext = { "!" , "#" ,  
                        "$" , "%" ,    
                        "&", "'" ,
                        "*" , "+" ,
                        "-" , "/" ,
                        "=" , "?" ,
                        "^" , "_" ,
                        "`" , "{" ,
                        "|" , "}" ,
                        "~", " ", "(",")"};

        private static readonly int MaxSizeName = 20;
        private static readonly int MinSizeName = 0;

        private static readonly int MaxDomainEmail = 20;
        private static readonly int MinDomainEmail = 0;

        private static readonly int MaxNameEmail = 20;
        private static readonly int MinNameEmail = 0;

        private static readonly int MaxCodeZone = 20;
        private static readonly int MaxPhoneNumberValue = 20;

        private static readonly int MaxNumPage = 999;
        private static readonly int CountSearchResultOnPage = 999;

        /// <summary>
        /// ID must be more then 0 and parse to Int32 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool VerifyProfileId(string id)
        {
            int r;
            return id!=null && int.TryParse(id, out r) && r >= 0;
        }

        public static bool VerifyEmail(string name, string domain)
        {
            if (string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(domain)) return false;
            return Validator.VerifyNameEmail(name) && Validator.VerifyDomainEmail(domain);
        }
        private static bool VerifyNameEmail(string name)
        {
            return name.Length <= MaxNameEmail && name.Length > MinNameEmail;
        }
        private static bool VerifyDomainEmail(string domain)
        {
            return domain.Length <= MaxDomainEmail && domain.Length > MinDomainEmail;
        }
        public static bool VerifyProfileName(string name)
        {
            if (name==null)
            {
                return false;
            }
            return !Atext.Any(name.Contains) && name.Length <= MaxSizeName && name.Length > MinSizeName;
        }
        public static bool VerifyPhoneNumber(string codeZone, string value)
        {
            if (codeZone == null || value==null)
            {
                return false;
            }
            return (codeZone.Any(Char.IsDigit) && codeZone.Length < MaxCodeZone) && (value.Any(Char.IsDigit) && value.Length < MaxPhoneNumberValue);
        }

        public static bool VerifySearchResultSize(int numPage, int countOnPage)
        {
            return numPage >= 0 && countOnPage >= 0;
        }

    }
}
