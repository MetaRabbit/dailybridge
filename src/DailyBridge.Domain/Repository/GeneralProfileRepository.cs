﻿using System;
using System.Linq;
using System.Threading.Tasks;
using DailyBridge.Domain.Identity;
using DailyBridge.Domain.Interface.Entity;
using DailyBridge.Domain.Interface.Enumeration;
using DailyBridge.Domain.Interface.Identity;
using DailyBridge.Domain.Interface.Service;
using DailyBridge.Domain.Service;
using DailyBridge.Domain.ValueObject;
using DailyBridge.Infrastruction.Interface;
using DailyBridge.Infrastruction.Utils;
using Data;
using Microsoft.Data.Entity;
using GeneralProfile = DailyBridge.Domain.Entity.GeneralProfile;

namespace DailyBridge.Domain.Repository
{
    public class GeneralProfileRepository : IGeneralProfileRepository
    {
        private IPersonalRepository _personalRepository;
        private IPersonalLocationQueryService _locationQueryService;
        IPersonalEventQueryService _personalEventQueryService;
        IPartipationQueryService _partipationQueryService;
        INewsQueryService _newsQueryService;

        public GeneralProfileRepository(IPersonalRepository personalRepository, IPersonalLocationQueryService locationQueryService, IPersonalEventQueryService personalEventQueryService, IPartipationQueryService partipationQueryService, INewsQueryService newsQueryService)
        {
            _personalRepository = personalRepository;
            _locationQueryService = locationQueryService;
            _personalEventQueryService = personalEventQueryService;
            _partipationQueryService = partipationQueryService;
            _newsQueryService = newsQueryService;
        }

        #region Profile

        public async Task<bool> AddAsync(IGeneralProfile profile)
        {
            var data = GeneralProfileConvertService.ConvertToDto(profile);
            _personalRepository.GeneralProfile.Add(data);
            try
            {
                await _personalRepository.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }          
        }

        public async Task<IGeneralProfile> GetProfileAsync(IGeneralProfileId idUser)
        {

            var profile = await _personalRepository.GeneralProfile.FirstOrDefaultAsync(
                generalProfile => generalProfile.GeneralProfileId == Convert.ToInt32(idUser.Id));

            return GeneralProfileConvertService.ConvertToDomainOject(profile);
        }

        public async Task<bool> ExistsAsync(IGeneralProfileId id)
        {
            return await _personalRepository.GeneralProfile.FirstOrDefaultAsync(
                generalProfile => generalProfile.GeneralProfileId == Convert.ToInt32(id.Id)) != null;
        }

        public bool Exists(IGeneralProfileId id)
        {
            return _personalRepository.GeneralProfile.FirstOrDefault(
                generalProfile => generalProfile.GeneralProfileId == Convert.ToInt32(id.Id)) != null;
        }

        public async Task UpdateAsync(IGeneralProfile profile)
        {
            var data = _personalRepository.GeneralProfile.FirstOrDefault(
                generalProfile => generalProfile.GeneralProfileId == Convert.ToInt32(profile.ProfileId.Id));
            data.FirstName = profile.FullName.FirstName;
            data.LastName = profile.FullName.LastName;
            data.MiddleName = profile.FullName.MiddleName;
            data.Email = profile.Email.Full;
            data.PhoneNumber = profile.PhoneNumber.Value;
            _personalRepository.GeneralProfile.Update(data);
            await _personalRepository.SaveChangesAsync();
        }

        #endregion

        #region Contact

        public async Task<IContact> GetContactAsync(IContactId contact)
        {
            try
            {
                var contactDto = await _personalRepository.Contact.AsNoTracking().FirstOrDefaultAsync(
                                    contact1 =>
                                        contact1.Source.Equals(Convert.ToInt32(contact.SourceId.Id)) &&
                                        contact1.Target.Equals(Convert.ToInt32(contact.TargetId.Id)));
                return GeneralProfileConvertService.ConvertToDomainOject(contactDto);
            }
            catch (Exception)
            {
                return null;
            }

         
        }

        /// <summary>
        /// Попытка добавления контакта. Если не существует одного из профилей или контакт уже существует, метод вернут FALSE. 
        /// </summary>
        /// <param name="contact"></param>
        /// <returns></returns>
        public async Task<bool> TryAddContactAsync(IContact contact)
        {
            if (contact == null ) return false;

            try
            {
                _personalRepository.Contact.Add(GeneralProfileConvertService.ConvertToDto(contact));
                await _personalRepository.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
         
        }

        public async Task<bool> DeleteAsync(IContactId contact)
        {
            try
            {
                _personalRepository.Contact.Remove(GeneralProfileConvertService.ConvertToDto(await GetContactAsync(contact)));
                await _personalRepository.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
           
        }

        public async Task<bool> ExistsAsync(IContactId contactId)
        {
            return await _personalRepository.Contact.FirstOrDefaultAsync(
                contact1 =>
                    contact1.Source.Equals(Convert.ToInt32(contactId.SourceId.Id)) &&
                    contact1.Target.Equals(Convert.ToInt32(contactId.TargetId.Id))) != null;

        }

        public bool Exists(IContactId contactId)
        {
            return _personalRepository.Contact.FirstOrDefault(
                contact1 =>
                    contact1.Source.Equals(Convert.ToInt32(contactId.SourceId.Id)) &&
                    contact1.Target.Equals(Convert.ToInt32(contactId.TargetId.Id))) != null;

        }

        public async Task UpdateAsync(IContact contact)
        {
            try
            {
                var contactDto = _personalRepository.Contact.FirstOrDefault(
                    contact1 =>
                        contact1.Source.Equals(Convert.ToInt32(contact.ContactId.SourceId.Id)) &&
                        contact1.Target.Equals(Convert.ToInt32(contact.ContactId.TargetId.Id)));
                contactDto.Status = (int)contact.Status;
                _personalRepository.Contact.Update(contactDto);
                await _personalRepository.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }


        }


        #endregion

        #region Event

        public async Task<IPersonalEvent> GetEventAsync(IPersonalEventId personalEventId)
        {
            if (personalEventId==null)
            {
                return null;
            }
            var personalEvent = await _personalEventQueryService.GetEventAsync(Convert.ToInt32(personalEventId.Id));

            return personalEvent == null ? null : GeneralProfileConvertService.ConvertToDomainOject(personalEvent);
        }

        public async Task<int> AddAsync(IPersonalEvent personalEvent, IParticipation participation)
        {
            if (personalEvent == null || participation == null)
            {
                return -1;
            }
            var dto = GeneralProfileConvertService.ConvertToDto(personalEvent);
            var dtoPart = GeneralProfileConvertService.ConvertToDto(participation);
            int eventId = await _personalRepository.FastInsertAsync(dto, dtoPart);
            return eventId;
            
        }

        public async Task<bool> DeleteAsync(IPersonalEventId personalEventId)
        {

           return await _personalRepository.FastDeletePersonalEventAsync(Convert.ToInt32(personalEventId.Id));
           
        }

        public Task<bool> ExistsAsync(IGeneralProfileId id, IPersonalEvent personalEvent)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> UpdateAsync(IPersonalEvent profile, bool p)
        {
            var dto = GeneralProfileConvertService.ConvertToDto(profile);
            bool success;
            if (p)
            {
                var newsDto = new News()
                {
                    PersonalEventId = dto.PersonalEventId,
                    TimeOffset = dto.TimeOffset,
                    Type = (int) TypeNews.Notice,
                    TypeNotify = (int) TypeNotify.Info
                };
                success = await _personalRepository.FastUpdateAsync(dto, newsDto);
            }
            else
            {
                success = await _personalRepository.FastUpdateAsync(dto, null);
            }
            return success;
        }

        public async Task<bool> AddNotice(IPersonalEventId personalEventId, long timeoffset)
        {
            var newsDto = new News() {PersonalEventId = Convert.ToInt32(personalEventId.Id), TimeOffset = timeoffset, Type = (int)TypeNews.Notice, TypeNotify = (int)TypeNotify.Info};
            bool success = await _personalRepository.FastInsertAsync(newsDto);
            return success;
        }

        #endregion

        #region Location

        public Task<IPersonalLocation> GetLocationsAsync(IGeneralProfile id)
        {
            throw new NotImplementedException();
        }

        public async Task<IPersonalLocation> GetLocationAsync(ILocationId id)
        {
            if (id == null)
            {
                return null;
            }

            var contactDto = await _locationQueryService.GetPersonalLocationAsync(Convert.ToInt32(id.Id));
            return contactDto == null ? null : GeneralProfileConvertService.ConvertToDomainOject(contactDto);
        }

        public async Task<bool> AddAsync(IPersonalLocation personalEventLocation)
        {
            if (personalEventLocation == null)
            {
                return false;
            }


            var data = GeneralProfileConvertService.ConvertToDto(personalEventLocation);

            return _personalRepository.FastInsertAsync(data).Result;
        }

        public async Task UpdateAsync(IPersonalLocation location)
        {
            if (location == null)
            {
                return;
            }
            var data = GeneralProfileConvertService.ConvertToDto(location);
            await _personalRepository.FastUpdateAsync(data);

        }

        public async Task<bool> DeleteAsync(IGeneralProfileId profId, ILocationId id)
        {
            if (id == null)
            {
                return false;
            }

            return await _personalRepository.FastDeletePersonalLocationAsync(Convert.ToInt32(id.Id), Convert.ToInt32(profId.Id));
        }

        public async Task<bool> AddAsync(IGeneralProfileId profileId, INews news)
        {
            var particiaption =  await _partipationQueryService.GetParticipation(Convert.ToInt32(news.PersonalEventId.Id),
                Convert.ToInt32(profileId.Id));
         
            if((TypeParticipation)particiaption.Type == TypeParticipation.Active || ((TypeParticipation)particiaption.Type) == TypeParticipation.Creater)
            {
               return  await _personalRepository.FastInsertAsync(GeneralProfileConvertService.ConvertToDto(news));
            }
            return false;
        }

        public async Task<bool> UpdateAuthorPrivilegesAsync(IGeneralProfileId profileId, INews news)
        {
            //Проверяем авторство новости
            if (!news.AuthorId.Id.Equals(profileId.Id))
            {
                return false;
            }
            return await _personalRepository.FastUpdateAsync(GeneralProfileConvertService.ConvertToDto(news), Convert.ToInt32(profileId.Id));
        }

        public async Task<bool> UpdateCreaterPrivilegesAsync(IGeneralProfileId profileId, INews news)
        {
            var particiaption =
                await _partipationQueryService.GetParticipation(Convert.ToInt32(news.PersonalEventId.Id),
                    Convert.ToInt32(profileId.Id));
            if ((TypeParticipation)particiaption.Type == TypeParticipation.Creater)
            {
                return await _personalRepository.FastUpdateAsync(GeneralProfileConvertService.ConvertToDto(news),Convert.ToInt32(profileId.Id));
            }
            return false;

        }

        public Task<bool> AddAsync(IParticipation personalEvent)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
