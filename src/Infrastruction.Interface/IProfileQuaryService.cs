﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data;

namespace DailyBridge.Infrastruction.Interfaces
{
    public interface IProfileQuaryService
    {
      Task<string> GetProfile(int id);
      Task<string> SearchProfiles(string text, int numPage, int countProfileOnPage);
      Task<string> SearchProfiles(string text, IList<int> selectedUserIds, int numPage, int countProfileOnPage);

    }
}
