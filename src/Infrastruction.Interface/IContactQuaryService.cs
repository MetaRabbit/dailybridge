﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DailyBridge.Infrastruction.Interfaces
{
    public interface IContactQuaryService
    {
        Task<string> GetContactsList(int id, int pageNumber, int contactOnPage);

    }
}
