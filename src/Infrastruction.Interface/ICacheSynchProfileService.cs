﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data;

namespace DailyBridge.Infrastruction.Interfaces
{
    public interface ICacheSynchProfileService
    {
        void SyncData(GeneralProfile profile);
        bool TrySyncDb(int id);
    }
}
