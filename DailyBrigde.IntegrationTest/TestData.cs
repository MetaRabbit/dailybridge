﻿using System.Collections.Generic;
using Data;


namespace DailyBrigde.IntegrationTest
{
    public class TestData
    {
        public  static readonly List<GeneralProfile> TestGeneralProfiles = new List<GeneralProfile>()
        {
            new GeneralProfile()
            {
            Email = "s",
            FirstName = "te",
            LastName = "Pit",
            MiddleName = "Pime",
            PhoneNumber = "2312",
            Status = 1,
            GeneralProfileId = 9001
            }
        }; 
    }
}
