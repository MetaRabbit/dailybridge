﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using DailyBridge;
using Data;
using Microsoft.AspNet.TestHost;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xunit;

namespace DailyBrigde.IntegrationTest.Identity
{
    public class ProfilesTests
    {
        private readonly TestServer _server;
        private readonly HttpClient _client;
        private const string RequestPath = "/api/GeneralProfile";
        public ProfilesTests()
        {
            // Arrange
            _server = new TestServer(TestServer.CreateBuilder()
                .UseStartup<Startup>());
            _client = _server.CreateClient();
        }

        private async Task<string> GetProfileResponseString(
            string querystring = "")
        {
            string request = "/api/GeneralProfile/1";
            if (!String.IsNullOrEmpty(querystring))
            {
                request += "?" + querystring;
            }
            var response = await _client.GetAsync(request);
            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsStringAsync();
        }
        [Fact]
        private void CreateProfile()
        {
            var profile = JsonConvert.SerializeObject(TestData.TestGeneralProfiles[0]);    
            HttpContent contentPost = new StringContent(profile, Encoding.UTF8, "application/json");
            var st =_client.PutAsync(RequestPath, contentPost).Result;
            Assert.Equal(HttpStatusCode.OK, st.StatusCode);
        }
       // [Fact]
        private void UpdateProfile()
        {
            var profileSer = JsonConvert.SerializeObject(new GeneralProfile()
            {
                Email = "AfterUpdateEmail",
                FirstName = "AfterUpda",
                LastName = "AfterUpdateLastName",
                MiddleName = "AfterUpdateMiddleName",
                PhoneNumber = "AfterUpdatePhoneNumber",
                Status = 1,
                GeneralProfileId = 9000
            });
            string request = "/api/GeneralProfile";
            HttpContent contentPost = new StringContent(profileSer, Encoding.UTF8, "application/json");
            var st = _client.PostAsync(request, contentPost).Result;
            Assert.Equal(HttpStatusCode.OK, st.StatusCode);

            var profile = GetProfile(9000);


            JObject jObject =JObject.Parse(profile);
            var res = jObject["fio"].ToObject<string>(JsonSerializer.CreateDefault());
            Assert.Equal(res.Equals("AfterUpda AfterUpdateLastName AfterUpdateMiddleName"), true);
        }
        private string GetProfile(int id)
        {
            var profile = _client.GetAsync(RequestPath+"/"+id).Result.Content.ReadAsStringAsync().Result;
            return profile;
        }
        //[Fact]
        private string SearchProfile()
        {
            var profile2 = GetProfile(10000);
            var profile3 = GetProfile(10001);
            Console.WriteLine(profile2);
            Console.WriteLine(profile3);
            var profile = _client.GetAsync(RequestPath + "?fuzzy=Афонькин&numPage=0&countProfileOnPage=50").Result.Content.ReadAsStringAsync().Result;
            return profile;
        }
        private void DeleteProfile(int id)
        {
            var profile = _client.DeleteAsync(RequestPath + "/" + id).Result.Content.ReadAsStringAsync().Result;
            
        }
    }
}
