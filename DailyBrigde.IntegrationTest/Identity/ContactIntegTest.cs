﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using DailyBridge;
using Microsoft.AspNet.TestHost;
using Newtonsoft.Json;
using Xunit;

namespace DailyBrigde.IntegrationTest.Identity
{
    public class ContactIntegTest
    {
        private readonly TestServer _server;
        private readonly HttpClient _client;
        private const string RequestPath = "/api/Contact";
        public ContactIntegTest()
        {
            // Arrange
            _server = new TestServer(TestServer.CreateBuilder()
                .UseStartup<Startup>());
            _client = _server.CreateClient();
        }
        //[Fact]
        private void CreateContact()
        {
            var profile = JsonConvert.SerializeObject(new { target = "10003" });
            HttpContent contentPost = new StringContent(profile, Encoding.UTF8, "application/json");
            var st = _client.PutAsync(RequestPath + "?id=10002", contentPost).Result;
            Assert.Equal(HttpStatusCode.OK, st.StatusCode);

        }
    }
}
